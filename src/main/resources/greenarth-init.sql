INSERT INTO master_categories (`category_id`, `created_by`, `creation_time`, `modification_time`, `modified_by`, `description`, `name`)
VALUES
	(1, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Home', 'Home'),
	(2, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Health & Wellness', 'Health & Wellness'),
	(3, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Beauty', 'Beauty'),
	(4, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Bathroom', 'Bathroom'),
	(5, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Kitchen', 'Kitchen'),
	(6, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Laundry & Cleaning', 'Laundry & Cleaning'),
	(7, '1', '2021-03-13 02:02:03', '2021-03-13 02:02:03', '1', 'Storage & Packaging', 'Storage & Packaging');

INSERT INTO role (`id`, `created_by`, `creation_time`, `modification_time`, `modified_by`, `name`)
VALUES
	(1, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'BUYER'),
	(2, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'SELLER'),
	(3, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'ADMIN');


INSERT INTO user (`user_id`, `created_by`, `creation_time`, `modification_time`, `modified_by`, `email`, `enabled`, `name`, `password`, `photo`, `verified`, `wallet_balance`, `role_id`)
VALUES
	(1, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'admin@greenarth.com', 1, 'Admin', '$2a$10$AXFJTmWop5glbykUfFBjj.2FkS4jst8.Cqs7A7hTYu6g5cKC1phu2', NULL, 1, 0, 3),
	(2, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'seller@greenarth.com', 1, 'Seller', '$2a$10$AXFJTmWop5glbykUfFBjj.2FkS4jst8.Cqs7A7hTYu6g5cKC1phu2', NULL, 1, 0, 2),
	(3, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 'buyer@greenarth.com', 1, 'Buyer', '$2a$10$AXFJTmWop5glbykUfFBjj.2FkS4jst8.Cqs7A7hTYu6g5cKC1phu2', NULL, 1, 0, 1),
	(4, 'SYSTEM', '2021-04-12 19:02:48', '2021-04-12 19:02:48', 'SYSTEM', 'dgfsgd@sfgdd.co', 1, 'sfdgfsdg', '$2a$10$lE38MqANINF/B2ON81nNXOPFrOZeFF8Ogh61zYpJ7o3f64xr7Hjpe', NULL, 1, 0, 2);


INSERT INTO seller_store (`store_id`, `created_by`, `creation_time`, `modification_time`, `modified_by`, `enabled`, `gst_number`, `legal_name`, `name`, `pan_number`, `verified`, `user_id`)
VALUES
	(1, NULL, '2021-03-13 02:02:03', '2021-03-13 02:02:03', NULL, 1, 'test-gst', 'Test Store', 'Test Store', 'test-pan', 1, 2);



INSERT INTO `products` (`product_id`, `active`, `sku_code`, `brand`, `name`, `stock`, `mrp`, `discount`, `category_id`, `header`, `details`, `how_to_recycle`, `materials_used`, `store_id`, `created_by`, `creation_time`, `modified_by`, `modification_time`)
VALUES
	(1, 1, 'HEABRUGR-001', 'brush with bamboo', 'Bamboo Toothbrush - Adult', 36, 110, 19, 2, 'A toothbrush, reinvented. Made of bamboo, not plastic, this brush helps to eliminate one of the most prevalent forms of ocean plastic pollution, plus it looks insanely chic in your bathroom and you don’t have to put plastic in your mouth two times per day.', 'This toothbrush is the perfect replacement for plastic toothbrushes. Our plant-based bamboo toothbrush is made completely from plants: soft bristles, smooth handle, wrapper and box. \r\rFully compostable packaging. USDA Certified Biobased. Dimensions: Length is 7\", Width is 4\" and the Height 1\". Product of USA and China. ', 'Cardboard Box: 100% recyclable in paper recycling\r\rBamboo handle: 100% compostable\r\rBristles: Remove bristles and can be recycled wherever nylon is recycled', 'Bamboo,Castor Bean Nylon', 1, '1', '2021-04-04 01:40:50', '1', '2021-04-04 01:40:50');


INSERT INTO `product_images` (`created_by`, `creation_time`, `modification_time`, `modified_by`, `imageurl`, `product_id`)
VALUES
	('2', '2021-04-08 00:19:28', '2021-04-08 00:19:28', '2', 'https://storage.googleapis.com/download/storage/v1/b/techrospect-bucket/o/productImages%2Fpr-img-2021-04-08%2000:19:26.754?generation=1617821367639989&alt=media', 1),
	('2', '2021-04-08 19:21:17', '2021-04-08 19:21:17', '2', 'https://storage.googleapis.com/download/storage/v1/b/techrospect-bucket/o/productImages%2Fpr-img-2021-04-08%2019:21:15.774?generation=1617889877183103&alt=media', 1);

INSERT INTO blogs (created_by,creation_time,modification_time,modified_by,added_by,content,heading,imageurl) VALUES
	 ('1','2021-05-14 14:19:34','2021-05-14 14:19:36','1','Dhruva Jetly','<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:center;''><strong><u><span style="font-size:24px;line-height:115%;color:black;">Why Plastic Bags should be banned?</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;">In today&rsquo;s time, use of plastic bags is so convenient for all of us. From vegetable vendors, grocery stores to big malls, we will have our purchases in lots and lots of plastic bags. It really has become part of our modern lives. However, we must understand that our convenience of using these plastic bags costs highly to the environment and nature and in turn it adversely affects health of humans.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;">Many countries around the world have banned the use of plastic bags or have placed taxes on its use considering its grave impact on our environment. Here are 5 powerful reasons which indicate us that we should immediately ban the use of plastic bags.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><strong><u><span style="font-size:19px;line-height:115%;color:black;">1. Plastic bags pollute our water bodies and our land&nbsp;</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;">Usually plastic bags are lightweight and thus can be carried away to very long distances by either water or wind. Plastic bag travelling by wind trashes various land areas, while water drops them in various water bodies. These litters get caught up in between trees, fences and floats in water bodies and ultimately move to the oceans. Thus, plastic bags pollute our water bodies and land.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><strong><u><span style="font-size:19px;line-height:115%;color:black;">2. Plastic bags are harmful to marine life&nbsp;</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;">Our oceans and array of marine species are succumbing to the poison of plastic.&nbsp;</span><span style="font-size:19px;line-height:115%;color:black;background:white;">According to the United Nations, at least&nbsp;</span><a href="https://news.un.org/en/story/2016/12/547032-new-un-report-finds-marine-debris-harming-more-800-species-costing-countries" target="_blank"><span style="font-size:19px;line-height:115%;color:black;background:white;text-decoration:none;">800 species</span></a><span style="font-size:19px;line-height:115%;color:black;background:white;">&nbsp;worldwide are affected by marine debris, and as much as 80 percent of that litter is plastic. Fish, seabirds, sea turtles, and marine mammals can become entangled in or ingest plastic debris, causing suffocation, starvation, and drowning. Plastic bags are thus causing serious damage to the marine life.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><strong><u><span style="font-size:19px;line-height:115%;color:black;background:white;">3. Made from non-renewable sources, Plastic Bags highly contribute to Climate Change</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;background:white;">Plastic is mostly made from the material called polypropylene, which is manufactured from petroleum and natural gas. All these materials are non renewable fossil fuel based materials, and&nbsp;</span><span style="font-size:19px;line-height:115%;color:black;background:white;">through their extraction and even production, greenhouse gases are created, which further contribute to&nbsp;</span><a href="https://www.conserve-energy-future.com/what-is-global-warming-and-climate-change.php" target="_blank"><span style="font-size:19px;line-height:115%;color:black;background:white;text-decoration:none;">global climate change</span></a><span style="font-size:19px;line-height:115%;color:black;background:white;">.</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><strong><u><span style="font-size:19px;line-height:115%;color:black;background:white;">4. Plastic bags cause harm to health of humans&nbsp;</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;background:white;">Plastic bags have some pollutants such as PCBs (Polychlorinated Biphenyl) together with PAHs (Polycyclic Aromatic Hydrocarbons), which are hormone disrupting, which can disrupt normal functioning of hormones in the body. When the marine animals consume plastic bags thrown into the oceans, they also consume these chemicals. When humans consumes the seafood like fish and other marine animals, such chemicals eventually move into them and affecting their health. Humans can develop cancers or other serious diseases.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><strong><u><span style="font-size:19px;line-height:115%;color:black;background:white;">5. Plastic bags are expensive and hard to remove from the environment</span></u></strong></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:.25in;line-height:115%;text-align:justify;background:white;''><span style=''font-size:19px;font-family:"Calibri",sans-serif;color:black;''>The cost of cleaning plastic bags is around&nbsp;</span><span style="color:black;"><a href="https://1bagatatime.com/learn/plastic-bag-clean-costs/" target="_blank"><span style=''font-size:19px;font-family:"Calibri",sans-serif;color:black;text-decoration:none;''>17 cents per bag</span></a></span><span style=''font-size:19px;font-family:"Calibri",sans-serif;color:black;''>; thus, on average, the taxpayers end up paying about $88 each year just on&nbsp;</span><span style="color:black;"><a href="https://www.conserve-energy-future.com/30-brilliant-ways-towards-reducing-your-plastic-consumption.php" target="_blank"><span style=''font-size:19px;font-family:"Calibri",sans-serif;color:black;text-decoration:none;''>plastic bag waste</span></a></span><span style=''font-size:19px;font-family:"Calibri",sans-serif;color:black;''>. Cities and other jurisdictions spend millions of dollars each year to maintain the landfills. Therefore, taxpayers are spending their money on the storage of plastic bags forever. These plastic bags also cost 3-5 cents each. Thus, the plastic bags which are considered free are not free at all. &nbsp;Individuals pay a lot to purchase them and even for reusing them.&nbsp;</span></p>
<p style=''margin-right:0in;margin-left:0in;font-size:15px;font-family:"Calibri",sans-serif;margin-top:0in;margin-bottom:10.0pt;line-height:115%;text-align:justify;''><span style="font-size:19px;line-height:115%;color:black;background:white;">Considering all these harmful impacts of plastic bags, we should actually ban the usage of them. Instead, we should opt for reusable bags, so that we can create sustainable products. Manufacturing such reusable bags will also create new job opportunities and encourage sustainable life! &nbsp;</span></p>','Why Plastic Bags should be banned?','https://storage.googleapis.com/download/storage/v1/b/greenarth-bucket/o/test%2Fblog-2021-05-14%2014:19:33.031?generation=1621001973576738&alt=media');

INSERT INTO master_countries (created_by,creation_time,modification_time,modified_by,name) VALUES
	 ('SYSTEM','2021-03-13 02:02:03','2021-03-13 02:02:03','SYSTEM','US'),
	 ('SYSTEM','2021-03-13 02:02:03','2021-03-13 02:02:03','SYSTEM','India');

INSERT INTO masteruisections (created_by,creation_time,modification_time,modified_by,name,view_order) VALUES
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Business Info','1'),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Registered Address','2'),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Verification Documents','3');

INSERT INTO masteruifields (created_by,creation_time,modification_time,modified_by,label,place_holder,regex,required,`type`,view_order,country_id,section_id) VALUES
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Store name','Enter store name',NULL,1,'input','1',1,1),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Legal company name','Enter legal company name',NULL,1,'input','2',1,1),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','EIN','Enter EIN',NULL,1,'input','3',1,1),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Contact person name','Enter your contact person''s name',NULL,1,'input','4',1,1),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Contact person phone','Enter your contact person''s phone',NULL,1,'input','5',1,1),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Street address','Street address or P.O. Box',NULL,1,'input','1',1,2),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Apartment','Apartment, suite, unit, building, floor etc.',NULL,1,'input','2',1,2),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','City','Enter city here',NULL,1,'input','3',1,2),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','State','Select a state',NULL,1,'dropdown','4',1,2),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','ZIP code','Enter ZIP code here',NULL,1,'input','5',1,2);
INSERT INTO masteruifields (created_by,creation_time,modification_time,modified_by,label,place_holder,regex,required,`type`,view_order,country_id,section_id) VALUES
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Upload Certificate of Insurance ','Drop Certificate of Insurance here',NULL,1,'file upload','1',1,3),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Upload Business license','Drop Business license here',NULL,1,'file upload','2',1,3),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Upload W8 form','Drop W8 form here',NULL,1,'file upload','3',1,3),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Upload W9 form','Drop W9 form here',NULL,1,'file upload','4',1,3),
	 ('SYSTEM','2021-07-09 02:02:03','2021-07-09 02:02:03','SYSTEM','Upload EIN form','Drop EIN form here',NULL,1,'file upload','5',1,3);

