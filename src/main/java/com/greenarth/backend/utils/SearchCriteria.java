package com.greenarth.backend.utils;


public class SearchCriteria {
    private String key;
    private Object value;
    private GreenarthConstants.SearchOperation operation;
    private int min;
    private int max;

    public SearchCriteria() {
    }

    public SearchCriteria(String key, Object value, GreenarthConstants.SearchOperation operation) {
        this.key = key;
        this.value = value;
        this.operation = operation;
    }

    public SearchCriteria(String key, GreenarthConstants.SearchOperation operation, int min, int max) {
        this.key = key;
        this.operation = operation;
        this.min = min;
        this.max = max;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public GreenarthConstants.SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(GreenarthConstants.SearchOperation operation) {
        this.operation = operation;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }


}