package com.greenarth.backend.utils;

public class GreenarthConstants {

    public static final String ASC = "ASC";
    public static final String DESC = "DESC";


    public class ServerError {

        public static final String DUPLICATE_EMAIL = "Email already registered";
        public static final String BLOG_NOT_ADDED = "Error occurred while adding blog";
        public static final String BLOG_NOT_DELETED = "Error occurred while deleting blog";
        public static final String USER_NOT_ADDED = "Error occurred while adding new user";
        public static final String BLOG_NOT_FOUND = "No blog found";
        public static final String STORE_NOT_FOUND = "No store found ";
        public static final String STORE_NOT_ADDED = "Store not added";
        public static final String STORE_DETAILS_ALREADY_EXIST = "Duplicate store details";
        public static final String INVALID_ID = "Invalid id";
        public static final String PRODUCTS_NOT_FOUND = "Products not found";
        public static final String FILTERS_NOT_FOUND = "Filters not found";
        public static final String PRODUCT_NOT_FOUND = "Product not found";
        public static final String USER_NOT_FOUND = "User not found";
        public static final String USER_NOT_AUTHORIZED = "User role is not authorized for this action";
        public static final String BAD_REQUEST = "Bad Request";
        public static final String CART_NOT_FOUND = "Cart not found";
        public static final String ADDRESS_NOT_FOUND = "Address not found";
        public static final String COUNTRY_NOT_FOUND = "Country not found";
        public static final String EMPTY_CART = "Looks like your cart is empty. Add some items to proceed.";
    }

    public class ServerSuccess {

        public static final String BLOG_ADDED = "Blog added";
        public static final String BLOG_DELETED = "Blog deleted";
        public static final String BLOG_LIST = "List of all blogs";
        public static final String USER_ADDED = "User added";
        public static final String STORE_ADDED = "Store added";
        public static final String STORE_VERIFIED = "Store verified";
        public static final String PRODUCT_ADDED = "Product added";
        public static final String PRODUCT_LIST = "All product list";
        public static final String ALL_FILTERS = "All filters";
        public static final String PRODUCT_STATUS_UPDATED = "Product status updated";
        public static final String STORE_DETAILS = "Store details";
        public static final String PRODUCT_DETAILS = "Product details";
        public static final String CART_UPDATED = "Cart updated";
        public static final String CART_DETAILS = "Cart details";
        public static final String APPLICATION_LAUNCHED = "Application launched";
        public static final String ALL_STORES = "List of all stores";
        public static final String ADDRESS_UPDATED = "Address updated";
        public static final String ADDRESS_DETAILS = "Address details";
        public static final String COUNTRY_LIST = "Country list";
        public static final String STORE_UPDATED = "Store details updated";
        public static final String DOCUMENTS_ADDED = "Document uploaded";
        public static final String DOCUMENTS_UPDATED = "Document updated";
        public static final String SHIPPING_DETAILS = "Shipping details";
    }

    public class AddressType {
        public static final String SHIPPING = "SHIPPING";
        public static final String BILLING = "BILLING";
        public static final String STORE = "STORE";
    }

    public class RoleTypes {
        public static final String ADMIN = "ADMIN";
        public static final String SELLER = "SELLER";
        public static final String BUYER = "BUYER";
    }

    public class StoreVerificationStatus {
        public static final String NOT_SUBMITTED = "NOT SUBMITTED";
        public static final String PENDING_FOR_APPROVAL = "PENDING FOR APPROVAL";
        public static final String APPROVED = "APPROVED";
        public static final String REJECTED = "REJECTED";
        public static final String REVISION_NEEDED = "REVISION NEEDED";
    }

    public enum SearchOperation {
        CATEGORY, BETWEEN, BRAND, CATEGORY_SEARCH, NAME_SEARCH, TAG_SEARCH;
    }

    public class SortBy {

        public static final String TIME = "TIME";
        public static final String PRICE = "PRICE";
        public static final String BRAND = "BRAND";
        public static final String NAME = "NAME";
    }

    public class Keys {
        public static final String LAUNCHED = "LAUNCHED";
    }

    public class S3FolderNames {
        public static final String BLOG_FOLDER = "blogs";
        public static final String PRODUCT_IMAGES_FOLDER = "product-images";
        public static final String VERIFICATION_DOCUMENTS_FOLDER = "seller-verification-documents";

    }
}
