package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.AddressDTO;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.AddressRequest;
import com.greenarth.backend.model.AddressRequestList;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.services.AddressService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/auth/buyer")
public class BuyerController {

    @Autowired
    AddressService addressService;

    @PostMapping(value = "/address")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addOrEditAddress(@RequestBody AddressRequestList addressRequestList) {
        try {
            List<AddressDTO> addressDTOList = addressService.addOrEditAddress(addressRequestList);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(addressDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.ADDRESS_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }


    @GetMapping(value = "/address")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getAddress() {
        try {
            List<AddressDTO> addressDTOList = addressService.getAddress();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(addressDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.ADDRESS_DETAILS).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }


}
