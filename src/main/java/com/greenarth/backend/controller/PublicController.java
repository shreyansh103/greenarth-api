package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.CountryDTO;
import com.greenarth.backend.DTO.FiltersDTO;
import com.greenarth.backend.DTO.ProductDTO;
import com.greenarth.backend.DTO.UserDTO;
import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.ProductSearchSortFilterAndPageRequest;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.model.UserSignupRequest;
import com.greenarth.backend.services.AddressService;
import com.greenarth.backend.services.BlogService;
import com.greenarth.backend.services.ProductService;
import com.greenarth.backend.services.UserService;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/public")
public class PublicController {

    @Autowired
    BlogService blogService;

    @Autowired
    UserService usersService;

    @Autowired
    ProductService productService;

    @Autowired
    AddressService addressService;

    @GetMapping("/")
    public String index() {
        return "hello world";
    }

    @GetMapping(value = "/list/blogs")
    public Response getAllBlogs() {
        try {
            List<Blogs> blogs = blogService.getAllBlogs();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(blogs)
                    .withMessage(GreenarthConstants.ServerSuccess.BLOG_LIST).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.BLOG_NOT_FOUND).build();
        }
    }

    @PostMapping(value = "/signup")
    public Response addNewUser(@RequestBody UserSignupRequest userSignupRequest) {
        try {
            UserDTO userDto = usersService.addNewUser(userSignupRequest);
            if (null != userDto) {
                return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(userDto)
                        .withMessage(GreenarthConstants.ServerSuccess.USER_ADDED).build();
            } else {
                return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                        .withMessage(GreenarthConstants.ServerError.USER_NOT_ADDED).build();
            }
        } catch (GreenarthCustomException greenarthCustomException) {
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.CONFLICT.name())
                    .withStatusCode(HttpStatus.CONFLICT.value()).withMessage(greenarthCustomException.getMessage())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.USER_NOT_ADDED).build();
        }
    }

    @PostMapping(value = "/all/products")
    public Response getAllProducts(@RequestBody ProductSearchSortFilterAndPageRequest productSearchSortFilterAndPageRequest) {
        try {
            ResponseCount productDTOList = productService.getAllProductsForBuyer(productSearchSortFilterAndPageRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(productDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.PRODUCT_LIST).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @GetMapping(value = "/filters")
    public Response getAllFilters() {
        try {
            FiltersDTO filtersDTO = productService.getAllFilters();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(filtersDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.ALL_FILTERS).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.FILTERS_NOT_FOUND).build();
        }
    }

    @GetMapping(value = "/product")
    public Response getProductBySkuCode(@RequestParam String skuCode) {
        try {
            ProductDTO productDTO = productService.getProductBySkuCode(skuCode);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(productDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.PRODUCT_DETAILS).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.PRODUCT_NOT_FOUND).build();
        }
    }

    @GetMapping(value = "/country/list")
    public Response getCountryList() {
        try {
            ResponseCount countryDTOList = addressService.getCountryList();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(countryDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.COUNTRY_LIST).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.COUNTRY_NOT_FOUND).build();
        }
    }
}
