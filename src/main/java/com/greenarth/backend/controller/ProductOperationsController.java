package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.ProductDTO;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.ProductRequest;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.services.ProductService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/auth/operations")
public class ProductOperationsController {

    @Autowired
    ProductService productService;

    @PostMapping(value = "/add/product")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addNewProduct(@ModelAttribute ProductRequest productRequest) {
        try {
            ProductDTO productDTO = productService.addNewProduct(productRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(productDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.PRODUCT_ADDED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/all/products")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getAllProducts(@RequestParam(defaultValue = "0", required = false) int pageNo,
                                   @RequestParam(defaultValue = "1000", required = false) int pageSize,
                                   @RequestParam(defaultValue = "desc", required = false) String direction,
                                   @RequestParam(defaultValue = "time", required = false) String sortStr) {
        try {
            ResponseCount productDTOList = productService.getAllProducts(pageNo, pageSize, direction, sortStr);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(productDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.PRODUCT_LIST).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/product/status")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response enableDisableProduct(@RequestParam String skuCode, boolean active) {
        try {
            ProductDTO productDTO = productService.enableDisableProduct(skuCode, active);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(productDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.PRODUCT_STATUS_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }


}
