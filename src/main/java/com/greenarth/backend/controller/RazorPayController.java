//package com.greenarth.backend.controller;
//
//import com.greenarth.backend.exception.ConflictException;
//import com.greenarth.backend.exception.NotFoundException;
//import com.greenarth.backend.model.Response;
//import com.greenarth.backend.utils.GreenarthConstants;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//
//@RestController
//@RequestMapping(path = "/auth/payment")
//public class RazorPayController {
//
//    @Autowired
//    private RazorPayService razorpayService;
//
//
//    @PostMapping(value = "/initiate")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
//    public Response initiatePayment(@RequestBody PaymentRequest paymentRequest) {
//        try {
//            PaymentDTO paymentDTO = razorpayService.initiatePayment(paymentRequest);
//            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(paymentDTO)
//                    .withMessage(GreenarthConstants.ServerSuccess.PAYMENT_INITIATED).build();
//        } catch (NotFoundException e) {
//            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
//                    .withMessage(e.getMessage()).build();
//        } catch (ConflictException e) {
//            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
//                    .withMessage(e.getMessage()).build();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
//                    .withMessage(e.getMessage()).build();
//        }
//    }
//
//    @RequestMapping(value = "/complete", method = RequestMethod.POST)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
//    public ResponseEntity<Response> completeSubscriptionPayment(HttpServletRequest request,
//                                                                @RequestBody SubscriptionPaymentRequest subscriptionPaymentRequest) throws RazorpayException {
//
//        Response response = razorpayService.completeSubscriptionPayment(request, subscriptionPaymentRequest);
//        return ResponseEntity.status(response.getStatusCode()).body(response);
//    }
//}
