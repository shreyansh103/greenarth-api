package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.SellerStoreDTO;
import com.greenarth.backend.DTO.StoreProfileSectionsDTO;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.model.StoreFileUploadFieldsRequest;
import com.greenarth.backend.model.StoreRequest;
import com.greenarth.backend.services.SellerStoreService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/auth/seller")
public class SellerStoreController {

    @Autowired
    SellerStoreService sellerStoreService;

    @GetMapping(value = "/profile/form")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getStoreProfileForm() {
        try {
            List<StoreProfileSectionsDTO> storeProfileSectionsDTOList = sellerStoreService.getStoreProfileForm();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(storeProfileSectionsDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.STORE_DETAILS).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/add/store/profile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addStoreProfile(@RequestBody StoreRequest storeRequest) {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.addStoreProfile(storeRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.STORE_ADDED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/add/verification/documents")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addVerificationDocuments(@ModelAttribute StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.addVerificationDocuments(storeFileUploadFieldsRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.DOCUMENTS_ADDED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/revise/verification/documents")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response reviseVerificationDocuments(@ModelAttribute StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.reviseVerificationDocuments(storeFileUploadFieldsRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.DOCUMENTS_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/revise/store/profile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response editStoreProfile(@RequestBody StoreRequest storeRequest) {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.editStoreProfile(storeRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.STORE_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @GetMapping(value = "/store/profile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getStoreProfile() {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.getStoreProfile();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.STORE_DETAILS).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }
}
