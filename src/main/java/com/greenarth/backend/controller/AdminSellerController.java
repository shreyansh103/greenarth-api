package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.SellerStoreDTO;
import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.*;
import com.greenarth.backend.services.BlogService;
import com.greenarth.backend.services.SellerStoreService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/auth/admin")
public class AdminSellerController {

    @Autowired
    SellerStoreService sellerStoreService;

    @PostMapping(value = "/verify/store")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response verifyStore(@RequestBody VerifyStoreRequest verifyStoreRequest) {
        try {
            SellerStoreDTO sellerStore = sellerStoreService.verifyStore(verifyStoreRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(sellerStore)
                    .withMessage(GreenarthConstants.ServerSuccess.STORE_VERIFIED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @GetMapping(value = "/list/store")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getAllStores(@RequestParam(defaultValue = "0", required = false) int pageNo, @RequestParam(defaultValue = "100", required = false) int pageSize) {
        try {
            ResponseCount response = sellerStoreService.getAllStores(pageNo, pageSize);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(response)
                    .withMessage(GreenarthConstants.ServerSuccess.ALL_STORES).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }


}
