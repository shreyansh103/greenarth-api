package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.CartDTO;
import com.greenarth.backend.DTO.ShippingOptionsByStoreDTO;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.CartItemRequest;
import com.greenarth.backend.model.CartRequest;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.services.BuyerCartService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/auth/buyer")
public class BuyerCartController {

    @Autowired
    BuyerCartService buyerCartService;

    @PostMapping(value = "/add/item/cart")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addToCart(@RequestBody CartRequest cartRequest) {
        try {
            CartDTO cartDTO = buyerCartService.addToCart(cartRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(cartDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.CART_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/modify/item/quantity")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response modifyQuantityOfCartItem(@RequestBody CartItemRequest cartItemRequest) {
        try {
            CartDTO cartDTO = buyerCartService.modifyQuantityOfCartItem(cartItemRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(cartDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.CART_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/remove/item/cart")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response removeItemFromCart(@RequestBody CartItemRequest cartItemRequest) {
        try {
            CartDTO cartDTO = buyerCartService.removeItemFromCart(cartItemRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(cartDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.CART_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @PostMapping(value = "/sync/cart")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response syncCart(@RequestBody CartRequest cartRequest) {
        try {
            CartDTO cartDTO = buyerCartService.syncCart(cartRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(cartDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.CART_UPDATED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @GetMapping(value = "/cart")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getCart() {
        try {
            CartDTO cartDTO = buyerCartService.getCart();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(cartDTO)
                    .withMessage(GreenarthConstants.ServerSuccess.CART_DETAILS).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.CART_NOT_FOUND).build();
        }
    }

    @GetMapping(value = "/cart/shipping")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getShippingOptions() {
        try {
            List<ShippingOptionsByStoreDTO> shippingOptionsByStoreDTOList = buyerCartService.getShippingOptions();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(shippingOptionsByStoreDTOList)
                    .withMessage(GreenarthConstants.ServerSuccess.SHIPPING_DETAILS).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.CART_NOT_FOUND).build();
        }
    }

}
