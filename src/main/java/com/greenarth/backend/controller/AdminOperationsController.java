package com.greenarth.backend.controller;

import com.greenarth.backend.DTO.SellerStoreDTO;
import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.model.StoreRequest;
import com.greenarth.backend.services.AdminOperationService;
import com.greenarth.backend.services.SellerStoreService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/auth/admin")
public class AdminOperationsController {

    @Autowired
    AdminOperationService adminOperationService;

    @PostMapping(value = "/launch")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response launchApplication() {
        try {
            Map<String, Boolean> responseMap = adminOperationService.launchApplication();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(responseMap)
                    .withMessage(GreenarthConstants.ServerSuccess.APPLICATION_LAUNCHED).build();
        } catch (NotFoundException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (ConflictException e) {
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.CONFLICT.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(e.getMessage()).build();
        }
    }

    @GetMapping(value = "/launch")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response getLaunchApplicationStatus() {
        try {
            Map<String, Boolean> responseMap = adminOperationService.getLaunchApplicationStatus();
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(responseMap)
                    .withMessage(GreenarthConstants.ServerSuccess.APPLICATION_LAUNCHED).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name()).withStatusCode(HttpStatus.NOT_FOUND.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.BLOG_NOT_FOUND).build();
        }
    }
}
