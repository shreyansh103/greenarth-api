package com.greenarth.backend.controller;

import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.model.BlogRequest;
import com.greenarth.backend.model.Response;
import com.greenarth.backend.services.BlogService;
import com.greenarth.backend.utils.GreenarthConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/auth/admin")
public class AdminBlogController {

    @Autowired
    BlogService blogService;

    @PostMapping(value = "/add/blog")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response addNewBlog(@ModelAttribute BlogRequest blogRequest) {
        try {
            Blogs blogs = blogService.addNewBlog(blogRequest);
            return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(blogs)
                    .withMessage(GreenarthConstants.ServerSuccess.BLOG_ADDED).build();
        } catch (GreenarthCustomException greenarthCustomException) {
            return Response.ResponseBuilder.aResponse().withStatus(HttpStatus.NOT_FOUND.name())
                    .withStatusCode(HttpStatus.NOT_FOUND.value()).withMessage(greenarthCustomException.getMessage())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.BLOG_NOT_ADDED).build();
        }
    }

    @DeleteMapping(value = "/delete/blog")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public Response deleteBlog(@RequestParam String blogId) {
        try {
            boolean isDeleted = blogService.deleteBlog(blogId);
            if (isDeleted) {
                return Response.ResponseBuilder.aResponse().withStatus("Success").withStatusCode(HttpStatus.OK.value()).withData(isDeleted)
                        .withMessage(GreenarthConstants.ServerSuccess.BLOG_DELETED).build();
            } else {
                return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(isDeleted)
                        .withMessage(GreenarthConstants.ServerError.BLOG_NOT_DELETED).build();

            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ResponseBuilder.aResponse().withStatus("Error").withStatusCode(HttpStatus.BAD_REQUEST.value()).withData(null)
                    .withMessage(GreenarthConstants.ServerError.BLOG_NOT_DELETED).build();
        }
    }


}
