package com.greenarth.backend;

import com.greenarth.backend.config.AuditorAwareImpl;
import com.greenarth.backend.repositories.UserRepository;
import com.greenarth.backend.services.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class GreenarthApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenarthApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Autowired
//    public void authenticationManager(AuthenticationManagerBuilder authenticationManagerBuilder, UserRepository repo) throws Exception {
//        authenticationManagerBuilder.userDetailsService(new UserDetailsService() {
//            @Override
//            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//                return new CustomUserDetails(repo.findByEmail(username).get());
//            }
//        }).passwordEncoder(passwordEncoder());
//
//    }

    @Bean
    public AuditorAwareImpl auditorProvider() {
        return new AuditorAwareImpl();
    }
}
