package com.greenarth.backend.exception;

public class ConflictException extends Exception {

    public ConflictException(String s) {
        super(s);
    }

}
