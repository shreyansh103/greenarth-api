package com.greenarth.backend.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greenarth.backend.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        Response errorResponse = Response.ResponseBuilder
                .aResponse()
                .withStatus(HttpStatus.FORBIDDEN.name())
                .withStatusCode(HttpStatus.FORBIDDEN.value())
                .withMessage("Access denied!")
                .build();
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), errorResponse);
        } catch (Exception e) {
            throw new ServletException();
        }
    }
}