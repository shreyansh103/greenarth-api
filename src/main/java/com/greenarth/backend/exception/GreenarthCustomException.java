package com.greenarth.backend.exception;

public class GreenarthCustomException extends Exception {

    public GreenarthCustomException(String s) {
        super(s);
    }

}
