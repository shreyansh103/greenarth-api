package com.greenarth.backend.exception;

import java.util.List;

public class InvalidImageExtensionException extends RuntimeException {
    List<String> validExtensions;

    public InvalidImageExtensionException(List<String> validExtensions) {
        this.validExtensions = validExtensions;
    }
}