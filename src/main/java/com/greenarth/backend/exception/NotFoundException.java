package com.greenarth.backend.exception;

public class NotFoundException extends Exception {

    public NotFoundException(String s) {
        super(s);
    }

}
