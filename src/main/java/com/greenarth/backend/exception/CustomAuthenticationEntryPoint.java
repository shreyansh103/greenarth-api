package com.greenarth.backend.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.greenarth.backend.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException)
            throws ServletException {

        Response errorResponse = Response.ResponseBuilder
                .aResponse()
                .withStatus(HttpStatus.UNAUTHORIZED.name())
                .withStatusCode(HttpStatus.UNAUTHORIZED.value())
                .withMessage(authException.getLocalizedMessage())
                .build();
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), errorResponse);
        } catch (Exception e) {
            throw new ServletException();
        }
    }
}