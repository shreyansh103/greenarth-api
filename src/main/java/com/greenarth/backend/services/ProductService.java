package com.greenarth.backend.services;

import com.greenarth.backend.DTO.FiltersDTO;
import com.greenarth.backend.DTO.ProductDTO;
import com.greenarth.backend.model.ProductRequest;
import com.greenarth.backend.model.ProductSearchSortFilterAndPageRequest;
import com.greenarth.backend.model.ResponseCount;

public interface ProductService {

    ProductDTO addNewProduct(ProductRequest productRequest) throws Exception;

    ResponseCount getAllProducts(int pageNo, int pageSize, String direction, String sortStr) throws Exception;

    ResponseCount getAllProductsForBuyer(ProductSearchSortFilterAndPageRequest productSearchSortFilterAndPageRequest) throws Exception;

    FiltersDTO getAllFilters() throws Exception;

    ProductDTO getProductBySkuCode(String skuCode) throws Exception;

    ProductDTO enableDisableProduct(String skuCode, boolean active) throws Exception;
}
