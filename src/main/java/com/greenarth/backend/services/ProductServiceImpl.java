package com.greenarth.backend.services;

import com.greenarth.backend.DTO.FiltersDTO;
import com.greenarth.backend.DTO.ProductDTO;
import com.greenarth.backend.entities.MasterCategories;
import com.greenarth.backend.entities.ProductImages;
import com.greenarth.backend.entities.Products;
import com.greenarth.backend.entities.User;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.ProductRequest;
import com.greenarth.backend.model.ProductSearchSortFilterAndPageRequest;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.repositories.CategtoriesRepository;
import com.greenarth.backend.repositories.ProductsRepository;
import com.greenarth.backend.repositories.SellerStoreRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import com.greenarth.backend.utils.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    UserService userService;

    @Autowired
    ProductsRepository productsRepository;

    @Autowired
    CategtoriesRepository categtoriesRepository;

    @Autowired
    SellerStoreRepository sellerStoreRepository;

    @Autowired
    AmazonClient amazonClient;

    @Override
    public ProductDTO addNewProduct(ProductRequest productRequest) throws Exception {
        User user = userService.getUser();
        if (null != productRequest.getProductId() && !productRequest.getProductId().equalsIgnoreCase("")) {
            Optional<Products> existingProduct = productsRepository.findById(Long.valueOf(productRequest.getProductId()));
            if (existingProduct.isPresent()) {
                if (null != productRequest.getProductImages() && productRequest.getProductImages().size() > 0) {
                    int counterOrder = 0;
                    for (MultipartFile file : productRequest.getProductImages()) {
//                        String imageURL = GCPClient.uploadObject("pr-img-" + new Timestamp(System.currentTimeMillis()), file, "productImages/");
                        String imageURL = amazonClient.uploadFileToBucket("pr-img" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.PRODUCT_IMAGES_FOLDER, file, true);

                        ProductImages productImage = new ProductImages();
                        productImage.setImageURL(imageURL);
                        existingProduct.get().addImage(productImage);
                        Products updatedProduct = productsRepository.save(existingProduct.get());
                        return ProductDTO.convertEntityToDTO(updatedProduct);
                    }
                } else {
                    if (null != productRequest.getBrand()) {
                        existingProduct.get().setBrand(productRequest.getBrand());
                    }
                    if (productRequest.getCategoryId() != 0) {
                        existingProduct.get().setCategory(categtoriesRepository.findById(productRequest.getCategoryId()).get());
                    }
                    if (null != productRequest.getDetails()) {
                        existingProduct.get().setDetails(productRequest.getDetails());
                    }
                    if (null != productRequest.getDiscount()) {
                        existingProduct.get().setDiscount(new BigDecimal(productRequest.getDiscount()));
                    }
                    if (null != productRequest.getHeader()) {
                        existingProduct.get().setHeader(productRequest.getHeader());
                    }
                    if (null != productRequest.getHowToRecycle()) {
                        existingProduct.get().setHowToRecycle(productRequest.getHowToRecycle());
                    }
                    if (null != productRequest.getName()) {
                        existingProduct.get().setName(productRequest.getName());
                    }
                    if (null != productRequest.getMrp()) {
                        existingProduct.get().setMrp(new BigDecimal(productRequest.getMrp()));
                    }
                    if (null != productRequest.getMaterialsUsed()) {
                        existingProduct.get().setMaterialsUsed(productRequest.getMaterialsUsed());
                    }
                    existingProduct.get().setStock(productRequest.getStock());
                    if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.ADMIN) && null != String.valueOf(productRequest.getStoreId())) {
                        existingProduct.get().setStore(sellerStoreRepository.findById(productRequest.getStoreId()).get());
                    }
                    if (null == existingProduct.get().getSkuCode() && null != existingProduct.get().getCategory() && null != existingProduct.get().getBrand()) {
                        existingProduct.get().setSkuCode(skuGenerator(existingProduct.get().getCategory().getName(), existingProduct.get().getBrand(), existingProduct.get().getProductId()));
                    }
                    Products updatedProduct = productsRepository.save(existingProduct.get());
                    return ProductDTO.convertEntityToDTO(updatedProduct);
                }
            } else {
                throw new NotFoundException(GreenarthConstants.ServerError.STORE_NOT_FOUND);
            }
            return ProductDTO.convertEntityToDTO(existingProduct.get());
        } else {
            Products product = new Products();
            for (MultipartFile file : productRequest.getProductImages()) {
//                String imageURL = GCPClient.uploadObject("pr-img-" + new Timestamp(System.currentTimeMillis()), file, "productImages/");
                String imageURL = amazonClient.uploadFileToBucket("pr-img" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.PRODUCT_IMAGES_FOLDER, file, true);
                ProductImages productImage = new ProductImages();
                productImage.setImageURL(imageURL);
                product.addImage(productImage);
            }
            product.setBrand(productRequest.getBrand());
            product.setCategory(productRequest.getCategoryId() != 0 ? categtoriesRepository.findById(productRequest.getCategoryId()).get() : null);
            product.setDetails(productRequest.getDetails());
            product.setDiscount(null != productRequest.getDiscount() ? new BigDecimal(productRequest.getDiscount()) : null);
            product.setHeader(productRequest.getHeader());
            product.setHowToRecycle(productRequest.getHowToRecycle());
            product.setName(productRequest.getName());
            product.setMrp(null != productRequest.getMrp() ? new BigDecimal(productRequest.getMrp()) : null);
            product.setMaterialsUsed(productRequest.getMaterialsUsed());
            product.setStock(productRequest.getStock());
            if
            (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.ADMIN) && null != String.valueOf(productRequest.getStoreId())) {
                product.setStore(sellerStoreRepository.findById(productRequest.getStoreId()).get());
            } else if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.SELLER)) {
                product.setStore(user.getSellerStore());
            }
            Products savedProduct = productsRepository.save(product);
            if (null != savedProduct.getCategory() && null != savedProduct.getBrand()) {
                savedProduct.setSkuCode(skuGenerator(savedProduct.getCategory().getName(), savedProduct.getBrand(), savedProduct.getProductId()));
                savedProduct = productsRepository.save(savedProduct);
            }
            return ProductDTO.convertEntityToDTO(savedProduct);
        }
    }

    @Override
    public ProductDTO enableDisableProduct(String skuCode, boolean active) throws Exception {
        Products product = productsRepository.findBySkuCode(skuCode);
        product.setActive(active);
        Products save = productsRepository.save(product);
        return ProductDTO.convertEntityToDTO(save);
    }

    @Override
    public ResponseCount getAllProducts(int pageNo, int pageSize, String direction, String sortStr) throws Exception {
        ResponseCount responseCount = null;
        User user = userService.getUser();
        String sortBy = "";
        Sort.Direction dir = null;
        if (null != direction
                && direction.equalsIgnoreCase(GreenarthConstants.ASC)) {
            dir = Sort.Direction.ASC;
        } else {
            dir = Sort.Direction.DESC;
        }

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(dir, sortStr));
        Page<Products> productsPage = null;
        long count = 0;
        if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.ADMIN)) {
            productsPage = productsRepository.findAll(paging);
            count = productsRepository.count();
        } else if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.SELLER)) {
            productsPage = productsRepository.findAllByStore(user.getSellerStore(), paging);
            count = productsRepository.countByStore(user.getSellerStore());
        }
        if (productsPage.getContent().size() > 0) {
            List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
            for (Products product :
                    productsPage.getContent()) {
                productDTOList.add(ProductDTO.convertEntityToDTO(product));
            }
            responseCount = new ResponseCount();
            responseCount.setCount(count);
            responseCount.setDataList(productDTOList);
        }
        return responseCount;
    }

    @Override
    public ResponseCount getAllProductsForBuyer(ProductSearchSortFilterAndPageRequest productSearchSortFilterAndPageRequest) throws Exception {
        ResponseCount responseCount = null;
        if (null == productSearchSortFilterAndPageRequest.getSortStr() || productSearchSortFilterAndPageRequest.getSortStr().isEmpty()) {
            productSearchSortFilterAndPageRequest.setSortStr("creationTime");
        }

        if (null == productSearchSortFilterAndPageRequest.getPageNo() || productSearchSortFilterAndPageRequest.getPageNo().isEmpty()) {
            productSearchSortFilterAndPageRequest.setPageNo("0");
        }
        if (null == productSearchSortFilterAndPageRequest.getPageSize() || productSearchSortFilterAndPageRequest.getPageSize().isEmpty()) {
            productSearchSortFilterAndPageRequest.setPageSize("1000");
        }
        Sort.Direction dir = null;
        if (null != productSearchSortFilterAndPageRequest.getDir()
                && productSearchSortFilterAndPageRequest.getDir().equalsIgnoreCase(GreenarthConstants.ASC)) {
            dir = Sort.Direction.ASC;
        } else {
            dir = Sort.Direction.DESC;
        }

        Pageable paging = PageRequest.of(Integer.parseInt(productSearchSortFilterAndPageRequest.getPageNo()),
                Integer.parseInt(productSearchSortFilterAndPageRequest.getPageSize()), Sort.by(dir, productSearchSortFilterAndPageRequest.getSortStr()));
        Specification<Products> specification = Specification.where(null);
        if (null != productSearchSortFilterAndPageRequest.getCategoryId() && !productSearchSortFilterAndPageRequest.getCategoryId().equalsIgnoreCase("")) {
            ProductSpecification categorySpec = new ProductSpecification();
            categorySpec.add(new SearchCriteria("category", productSearchSortFilterAndPageRequest.getCategoryId(),
                    GreenarthConstants.SearchOperation.CATEGORY));
            specification = specification.and(categorySpec);
        }
        if (null != productSearchSortFilterAndPageRequest.getBrand() && !productSearchSortFilterAndPageRequest.getBrand().equalsIgnoreCase("")) {
            ProductSpecification brandSpec = new ProductSpecification();
            brandSpec.add(new SearchCriteria("brand", productSearchSortFilterAndPageRequest.getBrand(),
                    GreenarthConstants.SearchOperation.BRAND));
            specification = specification.and(brandSpec);
        }
        if (null != productSearchSortFilterAndPageRequest.getPriceMin() && !productSearchSortFilterAndPageRequest.getPriceMin().equalsIgnoreCase("")
                && null != productSearchSortFilterAndPageRequest.getPriceMax() && !productSearchSortFilterAndPageRequest.getPriceMax().equalsIgnoreCase("")) {
            ProductSpecification priceRangeSpec = new ProductSpecification();
            priceRangeSpec.add(new SearchCriteria("priceRange",
                    GreenarthConstants.SearchOperation.BETWEEN,
                    Integer.parseInt(productSearchSortFilterAndPageRequest.getPriceMin()), Integer.parseInt(productSearchSortFilterAndPageRequest.getPriceMax())));
            specification = specification.and(priceRangeSpec);
        }
        if (null != productSearchSortFilterAndPageRequest.getSearchStr()) {
            Specification<Products> specificationInner = Specification.where(null);

            ProductSpecification categorySearchSpec = new ProductSpecification();
            categorySearchSpec.add(new SearchCriteria("categorySearch", productSearchSortFilterAndPageRequest.getSearchStr(),
                    GreenarthConstants.SearchOperation.CATEGORY_SEARCH));

            ProductSpecification nameSearchSpec = new ProductSpecification();
            nameSearchSpec.add(new SearchCriteria("nameSearch", productSearchSortFilterAndPageRequest.getSearchStr(),
                    GreenarthConstants.SearchOperation.NAME_SEARCH));

            ProductSpecification tagSearchSpec = new ProductSpecification();
            tagSearchSpec.add(new SearchCriteria("tagSearch", productSearchSortFilterAndPageRequest.getSearchStr(),
                    GreenarthConstants.SearchOperation.TAG_SEARCH));
            specificationInner = specificationInner.or(categorySearchSpec).or(nameSearchSpec).or(tagSearchSpec);
            specification = specification.and(specificationInner);
        }
        Page<Products> productsPage = productsRepository.findAll(specification, paging);
        long count = productsRepository.count(specification);

        if (productsPage.getContent().size() > 0) {
            List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
            for (Products product :
                    productsPage.getContent()) {
                productDTOList.add(ProductDTO.convertEntityToDTO(product));
            }
            responseCount = new ResponseCount();
            responseCount.setCount(count);
            responseCount.setDataList(productDTOList);
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.PRODUCTS_NOT_FOUND);
        }

        return responseCount;
    }

    @Override
    public FiltersDTO getAllFilters() {
        List<MasterCategories> categoriesList = categtoriesRepository.findAll();
        List<String> brandList = productsRepository.findAllBrands();
        return FiltersDTO.convertEntityToDTO(categoriesList, brandList);
    }

    @Override
    public ProductDTO getProductBySkuCode(String skuCode) throws Exception {
        return ProductDTO.convertEntityToDTO(productsRepository.findBySkuCode(skuCode));
    }

    private String skuGenerator(String categoryName, String brand, long productId) {
        String sku = "";
        sku = categoryName.substring(0, 2).toUpperCase() + brand.substring(0, 2).toUpperCase() + "GR-00" + String.valueOf(productId);
        return sku.toUpperCase();
    }
}
