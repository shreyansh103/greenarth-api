package com.greenarth.backend.services;

import com.greenarth.backend.DTO.CartDTO;
import com.greenarth.backend.DTO.ShippingOptionsByStoreDTO;
import com.greenarth.backend.model.CartItemRequest;
import com.greenarth.backend.model.CartRequest;

import java.util.List;

public interface BuyerCartService {

    CartDTO addToCart(CartRequest cartRequest) throws Exception;

    CartDTO modifyQuantityOfCartItem(CartItemRequest cartItemRequest) throws Exception;

    CartDTO removeItemFromCart(CartItemRequest cartItemRequest) throws Exception;

    CartDTO syncCart(CartRequest cartRequest) throws Exception;

    CartDTO getCart() throws Exception;

    List<ShippingOptionsByStoreDTO> getShippingOptions() throws Exception;
}
