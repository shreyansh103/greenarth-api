package com.greenarth.backend.services;

import com.greenarth.backend.entities.MasterFlags;
import com.greenarth.backend.repositories.FlagsRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AdminOperationImpl implements AdminOperationService {

    @Autowired
    FlagsRepository flagsRepository;


    @Override
    public Map<String, Boolean> getLaunchApplicationStatus() {
        Map<String, Boolean> responseMap = new HashMap<>();
        MasterFlags masterFlag = flagsRepository.getByKey(GreenarthConstants.Keys.LAUNCHED);
        if (null != masterFlag) {
            responseMap.put(GreenarthConstants.Keys.LAUNCHED, masterFlag.isEnabled());
        } else {
            responseMap.put(GreenarthConstants.Keys.LAUNCHED, Boolean.FALSE);
        }
        return responseMap;
    }

    @Override
    public Map<String, Boolean> launchApplication() {
        Map<String, Boolean> responseMap = new HashMap<>();
        MasterFlags masterFlag = flagsRepository.getByKey(GreenarthConstants.Keys.LAUNCHED);
        if (null == masterFlag) {
            MasterFlags masterFlags = new MasterFlags();
            masterFlags.setKey(GreenarthConstants.Keys.LAUNCHED);
            masterFlags.setEnabled(Boolean.TRUE);
            flagsRepository.save(masterFlags);
            responseMap.put(GreenarthConstants.Keys.LAUNCHED, masterFlags.isEnabled());
        } else {
            masterFlag.setEnabled(Boolean.TRUE);
            flagsRepository.save(masterFlag);
            responseMap.put(GreenarthConstants.Keys.LAUNCHED, masterFlag.isEnabled());
        }
        return responseMap;
    }
}
