package com.greenarth.backend.services;

import com.greenarth.backend.DTO.AddressDTO;
import com.greenarth.backend.model.AddressRequestList;
import com.greenarth.backend.model.ResponseCount;

import java.util.List;

public interface AddressService {
    List<AddressDTO> addOrEditAddress(AddressRequestList addressRequestList) throws Exception;

    List<AddressDTO> getAddress() throws Exception;

    ResponseCount getCountryList() throws Exception;
}
