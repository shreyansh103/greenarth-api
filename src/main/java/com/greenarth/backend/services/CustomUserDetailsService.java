package com.greenarth.backend.services;

import com.greenarth.backend.entities.User;
import com.greenarth.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.UserDeniedAuthorizationException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    private HttpServletRequest request;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmail(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("Could not find the user" + username);
        }
        String role = request.getParameter("role");
        if (new CustomUserDetails(user.get()).authorities.contains(new SimpleGrantedAuthority(role.toUpperCase()))) {
            return new CustomUserDetails(user.get());
        } else {
            throw new UserDeniedAuthorizationException(username + " is not authorized for this role");
        }
    }
}
