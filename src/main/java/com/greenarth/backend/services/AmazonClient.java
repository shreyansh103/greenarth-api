package com.greenarth.backend.services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.greenarth.backend.exception.FileConversionException;
import com.greenarth.backend.exception.InvalidImageExtensionException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class AmazonClient {

    private AmazonS3 s3client;
    @Value("${amazon.s3.endpoint}")
    private String endpointUrl;
    @Value("${amazon.s3.bucket-name-public}")
    private String publicBucketName;
    @Value("${amazon.s3.bucket-name-private}")
    private String privateBucketName;
    @Value("${amazon.s3.access-key}")
    private String accessKey;
    @Value("${amazon.s3.secret-key}")
    private String secretKey;

    @PostConstruct
    private void init() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        this.s3client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_EAST_2)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    public String uploadFileToBucket(String uuid, String folderName, MultipartFile multipartFile, boolean isPublic) {
        String fileUrl;

        try {
            List<String> validExtensions = Arrays.asList("jpeg", "jpg", "png", "pdf", "doc", "docx");
            String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

            if (!validExtensions.contains(extension)) {
                throw new InvalidImageExtensionException(validExtensions);
            } else {
                File file = convertMultiPartToFile(multipartFile);
                String fileName = generateFileName(uuid, multipartFile);
                uploadFileTos3bucket(folderName, fileName, file, isPublic);
                file.delete();
                if (isPublic) {
                    fileUrl = endpointUrl + "/" + folderName + "/" + fileName;
                } else {
                    fileUrl = folderName + "/" + fileName;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileConversionException();
        }
        return fileUrl;
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private String generateFileName(String uuid, MultipartFile multiPart) {
        return uuid + "-" + multiPart.getOriginalFilename().replace(" ", "_");
    }

    private void uploadFileTos3bucket(String folderName, String fileName, File file, boolean isPublic) {
        if (isPublic) {
            s3client.putObject(new PutObjectRequest(publicBucketName, folderName + "/" + fileName, file)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
        } else {
            s3client.putObject(new PutObjectRequest(privateBucketName, folderName + "/" + fileName, file)
                    .withCannedAcl(CannedAccessControlList.Private));
        }
    }

    public String deleteFileFromS3Bucket(String fileUrl) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(publicBucketName, fileName));
        return "Successfully deleted";
    }

    public String generatePreSignedUrl(String objectKey) {
        String urls = null;
        try {
            if (objectKey == null) {
                return null;
            }


            Date expiration = new Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 1000 * 60 * 60 * 2; // Add 30 min.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(privateBucketName, objectKey);
            generatePresignedUrlRequest.setMethod(HttpMethod.GET);
            generatePresignedUrlRequest.setExpiration(expiration);

            URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
            urls = url.toString();

        } catch (AmazonServiceException exception) {
            exception.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return urls;
    }

}
