package com.greenarth.backend.services;

import com.greenarth.backend.DTO.SellerStoreDTO;
import com.greenarth.backend.DTO.StoreProfileFieldsDTO;
import com.greenarth.backend.DTO.StoreProfileSectionsDTO;
import com.greenarth.backend.entities.*;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.*;
import com.greenarth.backend.repositories.SellerStoreProfileRepository;
import com.greenarth.backend.repositories.SellerStoreRepository;
import com.greenarth.backend.repositories.MasterUIFieldsRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SellerStoreServiceImpl implements SellerStoreService {

    @Autowired
    SellerStoreRepository sellerStoreRepository;

    @Autowired
    SellerStoreProfileRepository sellerStoreProfileRepository;

    @Autowired
    MasterUIFieldsRepository masterUIFieldsRepository;

    @Autowired
    UserService userService;

    @Autowired
    AmazonClient amazonClient;


    @Override
    public SellerStoreDTO addStoreProfile(StoreRequest storeRequest) throws Exception {
        User user = userService.getUser();
        for (StoreInputFieldsRequest storeInputFieldsRequest : storeRequest.getStoreInputFieldsRequestList()) {
            SellerStoreProfile sellerStoreProfile = new SellerStoreProfile();
            sellerStoreProfile.setStoreProfileField(masterUIFieldsRepository.findById(Long.parseLong(storeInputFieldsRequest.getFieldId())).get());
            sellerStoreProfile.setFieldValue(storeInputFieldsRequest.getFieldValue());
            user.getSellerStore().addStoreProfile(sellerStoreProfile);
        }
        user.getSellerStore().setVerificationStatus(GreenarthConstants.StoreVerificationStatus.PENDING_FOR_APPROVAL);

        SellerStore savedSellerStore = sellerStoreRepository.save(user.getSellerStore());
        return SellerStoreDTO.convertEntityToDTO(savedSellerStore, amazonClient);
    }

    @Override
    public SellerStoreDTO addVerificationDocuments(StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) throws Exception {
        User user = userService.getUser();
//        String imageURL = GCPClient.uploadObject("seller-doc-" + user.getUserId() + new Timestamp(System.currentTimeMillis()),
//                storeFileUploadFieldsRequest.getFile(), "seller-verification-documents/");
        String imageURL = amazonClient.uploadFileToBucket("seller-doc" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.VERIFICATION_DOCUMENTS_FOLDER, storeFileUploadFieldsRequest.getFile(), false);

        Optional<MasterUIFields> masterUIFields = masterUIFieldsRepository.findById(Long.parseLong(storeFileUploadFieldsRequest.getFieldId()));
        SellerStoreProfile existingDocument = user.getSellerStore().getSellerStoreProfileSet().stream()
                .filter(sId -> sId.getStoreProfileField().getFieldId() == masterUIFields.get().getFieldId()).findAny().orElse(null);
        if (null == existingDocument) {
            SellerStoreProfile sellerStoreProfile = new SellerStoreProfile();
            sellerStoreProfile.setStoreProfileField(masterUIFields.get());
            sellerStoreProfile.setFieldValue(imageURL);
            user.getSellerStore().addStoreProfile(sellerStoreProfile);
        } else {
            existingDocument.setFieldValue(imageURL);
        }
        SellerStore savedSellerStore = sellerStoreRepository.save(user.getSellerStore());

        return SellerStoreDTO.convertEntityToDTO(savedSellerStore, amazonClient);
    }

    @Override
    public SellerStoreDTO reviseVerificationDocuments(StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) throws Exception {
        User user = userService.getUser();
        SellerStore existingStore = user.getSellerStore();
        for (SellerStoreProfile existingProfile : existingStore.getSellerStoreProfileSet()) {
            if (existingProfile.getStoreProfileField().getFieldId() == Long.parseLong(storeFileUploadFieldsRequest.getFieldId())) {
//                String imageURL = GCPClient.uploadObject("seller-doc-" + user.getUserId() + new Timestamp(System.currentTimeMillis()),
//                        storeFileUploadFieldsRequest.getFile(), "seller-verification-documents/");
                String imageURL = amazonClient.uploadFileToBucket("seller-doc" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.VERIFICATION_DOCUMENTS_FOLDER, storeFileUploadFieldsRequest.getFile(), false);
                existingProfile.setFieldValue(imageURL);
                existingStore.setVerificationStatus(GreenarthConstants.StoreVerificationStatus.PENDING_FOR_APPROVAL);
            }
        }
        SellerStore sellerStore = sellerStoreRepository.save(existingStore);
        return SellerStoreDTO.convertEntityToDTO(sellerStore, amazonClient);
    }

    @Override
    public SellerStoreDTO editStoreProfile(StoreRequest storeRequest) throws Exception {
        User user = userService.getUser();
        SellerStore existingStore = user.getSellerStore();
        if (null != existingStore && existingStore.getVerificationStatus().equalsIgnoreCase(GreenarthConstants.StoreVerificationStatus.REVISION_NEEDED)) {
            for (StoreInputFieldsRequest storeInputFieldsRequest : storeRequest.getStoreInputFieldsRequestList()) {
                for (SellerStoreProfile existingProfile : existingStore.getSellerStoreProfileSet()) {
                    if (existingProfile.getStoreProfileField().getFieldId() == Long.parseLong(storeInputFieldsRequest.getFieldId())) {
                        existingProfile.setFieldValue(storeInputFieldsRequest.getFieldValue());
                    }
                }
            }
            existingStore.setVerificationStatus(GreenarthConstants.StoreVerificationStatus.PENDING_FOR_APPROVAL);
            SellerStore sellerStore = sellerStoreRepository.save(existingStore);
            return SellerStoreDTO.convertEntityToDTO(sellerStore, amazonClient);
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.STORE_NOT_FOUND);
        }
    }


    @Override
    public SellerStoreDTO verifyStore(VerifyStoreRequest verifyStoreRequest) throws Exception {
        if (null != verifyStoreRequest.getStoreId() && !verifyStoreRequest.getStoreId().equalsIgnoreCase("")) {
            Optional<SellerStore> existingStore = sellerStoreRepository.findById(Long.parseLong(verifyStoreRequest.getStoreId()));
            if (existingStore.isPresent()) {
                if (verifyStoreRequest.getStatus().equalsIgnoreCase(GreenarthConstants.StoreVerificationStatus.APPROVED)) {
                    existingStore.get().setVerified(Boolean.TRUE);
                    existingStore.get().setEnabled(Boolean.TRUE);
                    existingStore.get().setComments(null != verifyStoreRequest.getComments() ? verifyStoreRequest.getComments() : null);
                    existingStore.get().setVerificationStatus(GreenarthConstants.StoreVerificationStatus.APPROVED);
                } else {
                    existingStore.get().setComments(null != verifyStoreRequest.getComments() ? verifyStoreRequest.getComments() : null);
                    existingStore.get().setVerificationStatus(verifyStoreRequest.getStatus().toUpperCase());
                }
                SellerStore sellerStore = sellerStoreRepository.save(existingStore.get());
                return SellerStoreDTO.convertEntityToDTO(sellerStore, amazonClient);
            } else {
                throw new NotFoundException(GreenarthConstants.ServerError.STORE_NOT_FOUND);
            }
        } else {
            throw new GreenarthCustomException(GreenarthConstants.ServerError.INVALID_ID);
        }
    }

    @Override
    public SellerStoreDTO getStoreProfile() throws Exception {
        User user = userService.getUser();
        if (null != user.getSellerStore()) {
            return SellerStoreDTO.convertEntityToDTO(user.getSellerStore(), amazonClient);
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.STORE_NOT_FOUND);
        }
    }

    @Override
    public ResponseCount getAllStores(int pageNo, int pageSize) throws Exception {
        ResponseCount responseCount = null;
        Pageable paging = PageRequest.of(pageNo,
                pageSize, Sort.by(Sort.Direction.DESC, "creationTime"));
        Page<SellerStore> sellerStorePage = sellerStoreRepository.findAll(paging);
        if (sellerStorePage.getContent().size() > 0) {
            List<SellerStoreDTO> sellerStoreDTOList = new ArrayList<SellerStoreDTO>();
            for (SellerStore sellerStore :
                    sellerStorePage.getContent()) {
                sellerStoreDTOList.add(SellerStoreDTO.convertEntityToDTO(sellerStore, amazonClient));
            }
            responseCount = new ResponseCount();
            responseCount.setCount(sellerStorePage.stream().count());
            responseCount.setDataList(sellerStoreDTOList);
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.STORE_NOT_FOUND);
        }
        return responseCount;
    }

    @Override
    public List<StoreProfileSectionsDTO> getStoreProfileForm() throws Exception {
        List<StoreProfileSectionsDTO> storeProfileSectionsDTOList = new ArrayList<>();

        User user = userService.getUser();

        List<MasterUIFields> masterUIFieldsList = masterUIFieldsRepository.findAllByCountry(user.getSellerStore().getCountry());

        Map<MasterUISections, List<MasterUIFields>> mapBySections =
                masterUIFieldsList.stream().collect(Collectors.groupingBy(field -> field.getSection()));

        for (Map.Entry<MasterUISections, List<MasterUIFields>> entry : mapBySections.entrySet()) {
            StoreProfileSectionsDTO storeProfileSectionsDTO = new StoreProfileSectionsDTO();
            storeProfileSectionsDTO.setSectionId(entry.getKey().getSectionId());
            storeProfileSectionsDTO.setName(entry.getKey().getName());
            storeProfileSectionsDTO.setViewOrder(entry.getKey().getViewOrder());

            List<StoreProfileFieldsDTO> storeProfileFieldsDTOList = new ArrayList<>();
            for (MasterUIFields masterUIFields : entry.getValue()) {
                storeProfileFieldsDTOList.add(StoreProfileFieldsDTO.convertEntityToDTO(masterUIFields));
            }
            storeProfileFieldsDTOList = storeProfileFieldsDTOList.stream().sorted((e1, e2) -> e1.getViewOrder().compareTo(e2.getViewOrder())).collect(Collectors.toList());
            storeProfileSectionsDTO.setFieldList(storeProfileFieldsDTOList);
            storeProfileSectionsDTOList.add(storeProfileSectionsDTO);
        }

        storeProfileSectionsDTOList = storeProfileSectionsDTOList.stream().sorted((e1, e2) ->
                e1.getViewOrder().compareTo(e2.getViewOrder())).collect(Collectors.toList());
        return storeProfileSectionsDTOList;
    }


}
