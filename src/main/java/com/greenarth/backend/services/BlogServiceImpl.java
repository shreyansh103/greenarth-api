package com.greenarth.backend.services;

import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.model.BlogRequest;
import com.greenarth.backend.repositories.BlogsRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    BlogsRepository blogsRepository;

    @Autowired
    AmazonClient amazonClient;

    @Override
    public List<Blogs> getAllBlogs() throws Exception {
        List<Blogs> blogList = blogsRepository.findAll(Sort.by(Sort.Direction.DESC, "creationTime"));
        return blogList;
    }

    @Override
    public Blogs addNewBlog(BlogRequest blogRequest) throws Exception {
        if (null == blogRequest.getBlogId() || blogRequest.getBlogId().isEmpty()) {
            Blogs blogs = new Blogs();
            blogs.setAddedBy(blogRequest.getAddedBy());
            blogs.setContent(blogRequest.getContent());
            blogs.setHeading(blogRequest.getHeading());
            String imageURL = amazonClient.uploadFileToBucket("blog" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.BLOG_FOLDER, blogRequest.getFile(), true);
//            String imageURL = GCPClient.uploadObject("blog-" + new Timestamp(System.currentTimeMillis()), blogRequest.getFile(), "test/");
            blogs.setImageURL(imageURL);
            Blogs savedBlog = blogsRepository.save(blogs);
            return savedBlog;
        } else {
            Optional<Blogs> existingBlog = blogsRepository.findById(Long.valueOf(blogRequest.getBlogId()));
            if (existingBlog.isPresent()) {
                if (null != blogRequest.getFile()) {
                    String imageURL = amazonClient.uploadFileToBucket("blog" + new Timestamp(System.currentTimeMillis()), GreenarthConstants.S3FolderNames.BLOG_FOLDER, blogRequest.getFile(), true);
//                    String imageURL = GCPClient.uploadObject("blog" + new Timestamp(System.currentTimeMillis()), blogRequest.getFile(), "test/");
                    existingBlog.get().setImageURL(imageURL);
                }
                if (null != blogRequest.getContent()) {
                    existingBlog.get().setContent(blogRequest.getContent());
                }
                if (null != blogRequest.getAddedBy()) {
                    existingBlog.get().setAddedBy(blogRequest.getAddedBy());
                }
                if (null != blogRequest.getHeading()) {
                    existingBlog.get().setHeading(blogRequest.getHeading());
                }
            } else {
                throw new GreenarthCustomException(GreenarthConstants.ServerError.BLOG_NOT_FOUND);
            }
            Blogs savedBlog = blogsRepository.save(existingBlog.get());
            return savedBlog;
        }

    }

    @Override
    public boolean deleteBlog(String blogId) throws Exception {
        blogsRepository.deleteById(Long.valueOf(blogId));
        if (!blogsRepository.findById(Long.valueOf(blogId)).isPresent()) {
            return true;
        } else {
            return false;
        }
    }
}
