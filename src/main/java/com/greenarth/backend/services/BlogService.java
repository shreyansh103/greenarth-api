package com.greenarth.backend.services;

import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.model.BlogRequest;

import java.util.List;

public interface BlogService {
    List<Blogs> getAllBlogs() throws Exception;

    Blogs addNewBlog(BlogRequest blogRequest) throws Exception;

    boolean deleteBlog(String blogId) throws Exception;
}
