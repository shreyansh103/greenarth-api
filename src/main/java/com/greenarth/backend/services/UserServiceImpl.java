package com.greenarth.backend.services;

import com.greenarth.backend.DTO.UserDTO;
import com.greenarth.backend.entities.Role;
import com.greenarth.backend.entities.SellerStore;
import com.greenarth.backend.entities.User;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.model.UserSignupRequest;
import com.greenarth.backend.repositories.MasterCountriesRepository;
import com.greenarth.backend.repositories.RoleRepository;
import com.greenarth.backend.repositories.UserRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    MasterCountriesRepository masterCountriesRepository;

    @Override
    public UserDTO addNewUser(UserSignupRequest userSignupRequest) throws Exception {
        UserDTO userDTO = null;
        if (!userRepository.existsByEmail(userSignupRequest.getEmail())) {
            Optional<Role> role = roleRepository.findByNameIgnoreCase(userSignupRequest.getRole());
            if (role.isPresent()) {
                User user = new User();
                user.setEmail(userSignupRequest.getEmail());
                user.setName(userSignupRequest.getName());
                user.setPassword(passwordEncoder.encode(userSignupRequest.getPassword()));
                user.setRole(role.get());
                if (role.get().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.SELLER)) {
                    SellerStore sellerStore = new SellerStore();
                    sellerStore.setEnabled(Boolean.FALSE);
                    sellerStore.setVerified(Boolean.FALSE);
                    sellerStore.setVerificationStatus(GreenarthConstants.StoreVerificationStatus.NOT_SUBMITTED);
                    sellerStore.setCountry(masterCountriesRepository.getOne(Long.parseLong(userSignupRequest.getCountryId())));
                    sellerStore.setUser(user);
                    user.setSellerStore(sellerStore);
                }
                userRepository.save(user);
                userDTO = new UserDTO();
                userDTO = userDTO.convertEntityToDTO(user);
            }
        } else {
            throw new GreenarthCustomException(GreenarthConstants.ServerError.DUPLICATE_EMAIL);
        }
        return userDTO;
    }

    public User getUser() {
        Optional<User> user = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String userEmail = ((UserDetails) principal).getUsername();
            user = userRepository.findByEmail(userEmail);
        }
        return user.get();
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
