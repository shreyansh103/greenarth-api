package com.greenarth.backend.services;

import java.util.Map;

public interface AdminOperationService {
    Map<String, Boolean> getLaunchApplicationStatus() throws Exception;

    Map<String, Boolean> launchApplication() throws Exception;
}
