package com.greenarth.backend.services;

import com.greenarth.backend.DTO.SellerStoreDTO;
import com.greenarth.backend.DTO.StoreProfileSectionsDTO;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.model.StoreFileUploadFieldsRequest;
import com.greenarth.backend.model.StoreRequest;
import com.greenarth.backend.model.VerifyStoreRequest;

import java.util.List;

public interface SellerStoreService {

    SellerStoreDTO addStoreProfile(StoreRequest storeRequest) throws Exception;

    SellerStoreDTO editStoreProfile(StoreRequest storeRequest) throws Exception;

    SellerStoreDTO verifyStore(VerifyStoreRequest verifyStoreRequest) throws Exception;

    SellerStoreDTO getStoreProfile() throws Exception;

    ResponseCount getAllStores(int pageNo, int pageSize) throws Exception;

    List<StoreProfileSectionsDTO> getStoreProfileForm() throws Exception;

    SellerStoreDTO addVerificationDocuments(StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) throws Exception;

    SellerStoreDTO reviseVerificationDocuments(StoreFileUploadFieldsRequest storeFileUploadFieldsRequest) throws Exception;
}
