package com.greenarth.backend.services;

import com.greenarth.backend.DTO.AddressDTO;
import com.greenarth.backend.DTO.CountryDTO;
import com.greenarth.backend.entities.Address;
import com.greenarth.backend.entities.MasterCountries;
import com.greenarth.backend.entities.User;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.AddressRequest;
import com.greenarth.backend.model.AddressRequestList;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.repositories.MasterCountriesRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    UserService userService;

    @Autowired
    MasterCountriesRepository masterCountriesRepository;


    @Override
    public List<AddressDTO> addOrEditAddress(AddressRequestList addressRequestList) throws Exception {
        User user = userService.getUser();
        List<AddressDTO> addressDTOList = new ArrayList<>();
        if (null != user) {
            for (AddressRequest addressRequest : addressRequestList.getAddressRequestList()) {
                if (null == addressRequest.getAddressId() || addressRequest.getAddressId().equalsIgnoreCase("")) {
                    Address address = new Address();
                    address.setAddressType(addressRequest.getAddressType());
                    address.setAddress(null != addressRequest.getAddress() ? addressRequest.getAddress() : null);
                    address.setStreetAddress(null != addressRequest.getStreetAddress() ? addressRequest.getStreetAddress() : null);
                    address.setApartment(null != addressRequest.getApartment() ? addressRequest.getApartment() : null);
                    address.setAddressTag(null != addressRequest.getAddressTag() ? addressRequest.getAddressTag() : null);
                    address.setAdditionalInformation(null != addressRequest.getAdditionalInformation() ? addressRequest.getAdditionalInformation() : null);
                    address.setCity(null != addressRequest.getCity() ? addressRequest.getCity() : null);
                    address.setLandmark(null != addressRequest.getLandmark() ? addressRequest.getLandmark() : null);
                    address.setState(null != addressRequest.getState() ? addressRequest.getState() : null);
                    address.setPinCode(null != addressRequest.getPinCode() ? addressRequest.getPinCode() : null);
                    address.setZipCode(null != addressRequest.getZipCode() ? addressRequest.getZipCode() : null);
                    address.setPhone(null != addressRequest.getPhone() ? addressRequest.getPhone() : null);
                    address.setName(null != addressRequest.getName() ? addressRequest.getName() : null);
                    address.setBillingSame(addressRequest.isBillingSame());
                    address.setCountry(masterCountriesRepository.getOne(addressRequest.getCountryId()));
                    user.addAddress(address);
                } else {
                    for (Address existingAddress : user.getAddressSet()) {
                        if (existingAddress.getAddressId() == Long.parseLong(addressRequest.getAddressId())) {

                            if (null != addressRequest.getAddress()) {
                                existingAddress.setAddress(addressRequest.getAddress());
                            }
                            if (null != addressRequest.getStreetAddress()) {
                                existingAddress.setStreetAddress(addressRequest.getStreetAddress());
                            }
                            if (null != addressRequest.getApartment()) {
                                existingAddress.setApartment(addressRequest.getApartment());
                            }
                            if (null != addressRequest.getAddressTag()) {
                                existingAddress.setAddressTag(addressRequest.getAddressTag());
                            }
                            if (null != addressRequest.getAdditionalInformation()) {
                                existingAddress.setAdditionalInformation(addressRequest.getAdditionalInformation());
                            }
                            if (null != addressRequest.getCity()) {
                                existingAddress.setCity(addressRequest.getCity());
                            }
                            if (null != addressRequest.getLandmark()) {
                                existingAddress.setLandmark(addressRequest.getLandmark());
                            }
                            if (null != addressRequest.getState()) {
                                existingAddress.setState(addressRequest.getState());
                            }
                            if (null != addressRequest.getPinCode()) {
                                existingAddress.setPinCode(addressRequest.getPinCode());
                            }
                            if (null != addressRequest.getZipCode()) {
                                existingAddress.setZipCode(addressRequest.getZipCode());
                            }
                            if (null != addressRequest.getPhone()) {
                                existingAddress.setPhone(addressRequest.getPhone());
                            }
                            if (null != addressRequest.getName()) {
                                existingAddress.setName(addressRequest.getName());
                            }
                        }
                    }
                }
                userService.saveUser(user);
                for (Address savedAddress : user.getAddressSet()
                ) {
                    addressDTOList.add(AddressDTO.convertEntityToDTO(savedAddress));
                }
                return addressDTOList;
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
        return addressDTOList;
    }

    @Override
    public List<AddressDTO> getAddress() throws Exception {
        User user = userService.getUser();
        if (null != user) {
            List<AddressDTO> addressDTOList = new ArrayList<>();
            for (Address savedAddress : user.getAddressSet()) {
                addressDTOList.add(AddressDTO.convertEntityToDTO(savedAddress));
            }
            return addressDTOList;
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }

    @Override
    public ResponseCount getCountryList() throws Exception {
        List<CountryDTO> countryDTOList = new ArrayList<>();
        ResponseCount responseCount = null;
        List<MasterCountries> masterCountriesList = masterCountriesRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
        if (masterCountriesList.size() > 0) {
            for (MasterCountries country : masterCountriesList) {
                countryDTOList.add(CountryDTO.convertEntityToDTO(country));
            }
            responseCount = new ResponseCount();
            responseCount.setCount((long) masterCountriesList.size());
            responseCount.setDataList(countryDTOList);
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.COUNTRY_NOT_FOUND);
        }
        return responseCount;
    }
}
