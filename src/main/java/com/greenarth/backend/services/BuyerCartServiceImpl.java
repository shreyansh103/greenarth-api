package com.greenarth.backend.services;

import com.greenarth.backend.DTO.*;
import com.greenarth.backend.entities.Cart;
import com.greenarth.backend.entities.CartItems;
import com.greenarth.backend.entities.SellerStore;
import com.greenarth.backend.entities.User;
import com.greenarth.backend.exception.ConflictException;
import com.greenarth.backend.exception.GreenarthCustomException;
import com.greenarth.backend.exception.NotFoundException;
import com.greenarth.backend.model.CartItemRequest;
import com.greenarth.backend.model.CartRequest;
import com.greenarth.backend.model.ResponseCount;
import com.greenarth.backend.repositories.CartRepository;
import com.greenarth.backend.repositories.ProductsRepository;
import com.greenarth.backend.utils.GreenarthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BuyerCartServiceImpl implements BuyerCartService {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    ProductsRepository productsRepository;

    @Autowired
    UserService userService;

    @Override
    public CartDTO addToCart(CartRequest cartRequest) throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.BUYER)) {
                if (null == user.getCart()) {
                    if (null != cartRequest.getCartProductRequestList() && cartRequest.getCartProductRequestList().size() > 0) {
                        Cart cart = new Cart();
                        for (CartItemRequest cartItemRequest : cartRequest.getCartProductRequestList()) {
                            CartItems cartItems = new CartItems();
                            cartItems.setProduct(productsRepository.findById(cartItemRequest.getProductId()).get());
                            cartItems.setQuantity(cartItemRequest.getQuantity());
                            cart.addCartItem(cartItems);
                        }
                        cart.setUser(user);
                        user.setCart(cart);
                        User savedUser = userService.saveUser(user);
                        return CartDTO.convertEntityToDTO(savedUser.getCart());
                    } else {
                        throw new GreenarthCustomException(GreenarthConstants.ServerError.BAD_REQUEST);
                    }
                } else {
                    if (null != cartRequest.getCartProductRequestList() && cartRequest.getCartProductRequestList().size() > 0) {
                        boolean cartUpdated = false;
                        for (CartItemRequest cartItemRequest : cartRequest.getCartProductRequestList()) {
                            for (CartItems cartItem : user.getCart().getCartItemsSet()) {
                                if (cartItem.getProduct().getProductId().equals(cartItemRequest.getProductId())) {
                                    cartItem.setQuantity(cartItem.getQuantity() + 1);
                                    cartUpdated = true;
                                }
                            }
                            if (!cartUpdated) {
                                CartItems cartItems = new CartItems();
                                cartItems.setProduct(productsRepository.findById(cartItemRequest.getProductId()).get());
                                cartItems.setQuantity(cartItemRequest.getQuantity());
                                user.getCart().addCartItem(cartItems);
                            }
                        }
                        Cart savedCart = cartRepository.save(user.getCart());
                        return CartDTO.convertEntityToDTO(savedCart);
                    } else {
                        throw new GreenarthCustomException(GreenarthConstants.ServerError.BAD_REQUEST);
                    }
                }
            } else {
                throw new ConflictException(GreenarthConstants.ServerError.USER_NOT_AUTHORIZED);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }

    }

    @Override
    public CartDTO modifyQuantityOfCartItem(CartItemRequest cartItemRequest) throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.BUYER)) {
                if (null != user.getCart()) {
                    for (CartItems cartItems : user.getCart().getCartItemsSet()) {
                        if (cartItems.getProduct().getProductId().equals(cartItemRequest.getProductId())) {
                            cartItems.setQuantity(cartItemRequest.getQuantity());
                        }
                    }
//                    Cart savedCart = cartRepository.save(user.getCart());
//                    return CartDTO.convertEntityToDTO(savedCart);
                    User savedUser = userService.saveUser(user);
                    return CartDTO.convertEntityToDTO(savedUser.getCart());
                } else {
                    throw new GreenarthCustomException(GreenarthConstants.ServerError.BAD_REQUEST);
                }
            } else {
                throw new ConflictException(GreenarthConstants.ServerError.USER_NOT_AUTHORIZED);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }

    @Override
    public CartDTO removeItemFromCart(CartItemRequest cartItemRequest) throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.BUYER)) {
                if (null != user.getCart()) {
                    Set<CartItems> tempCartItemsSet = new HashSet<CartItems>();
                    tempCartItemsSet.addAll(user.getCart().getCartItemsSet());
                    for (CartItems cartItems : tempCartItemsSet) {
                        if (cartItems.getProduct().getProductId().equals(cartItemRequest.getProductId())) {
                            user.getCart().removeCartItem(cartItems);
                        }
                    }
//                    Cart savedCart = cartRepository.save(user.getCart());
//                    return CartDTO.convertEntityToDTO(savedCart);
                    User savedUser = userService.saveUser(user);
                    return CartDTO.convertEntityToDTO(savedUser.getCart());
                } else {
                    throw new GreenarthCustomException(GreenarthConstants.ServerError.BAD_REQUEST);
                }
            } else {
                throw new ConflictException(GreenarthConstants.ServerError.USER_NOT_AUTHORIZED);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }

    @Override
    public CartDTO syncCart(CartRequest cartRequest) throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (user.getRole().getName().equalsIgnoreCase(GreenarthConstants.RoleTypes.BUYER)) {
                if (null != user.getCart()) {
                    if (user.getCart().getCartItemsSet().size() > 0) {
                        Set<CartItems> tempCartItemsSet = new HashSet<CartItems>();
                        tempCartItemsSet.addAll(user.getCart().getCartItemsSet());
                        for (CartItems cartItems : tempCartItemsSet) {
                            user.getCart().removeCartItem(cartItems);
                        }
                        for (CartItemRequest cartItemRequest : cartRequest.getCartProductRequestList()) {
                            CartItems cartItems = new CartItems();
                            cartItems.setProduct(productsRepository.findById(cartItemRequest.getProductId()).get());
                            cartItems.setQuantity(cartItemRequest.getQuantity());
                            user.getCart().addCartItem(cartItems);
                        }
                        User savedUser = userService.saveUser(user);
                        return CartDTO.convertEntityToDTO(savedUser.getCart());
                    } else {
                        for (CartItemRequest cartItemRequest : cartRequest.getCartProductRequestList()) {
                            CartItems cartItems = new CartItems();
                            cartItems.setProduct(productsRepository.findById(cartItemRequest.getProductId()).get());
                            cartItems.setQuantity(cartItemRequest.getQuantity());
                            user.getCart().addCartItem(cartItems);
                        }
                        User savedUser = userService.saveUser(user);
                        return CartDTO.convertEntityToDTO(savedUser.getCart());
                    }
                } else {
                    Cart cart = new Cart();
                    for (CartItemRequest cartItemRequest : cartRequest.getCartProductRequestList()) {
                        CartItems cartItems = new CartItems();
                        cartItems.setProduct(productsRepository.findById(cartItemRequest.getProductId()).get());
                        cartItems.setQuantity(cartItemRequest.getQuantity());
                        cart.addCartItem(cartItems);
                    }
                    cart.setUser(user);
                    user.setCart(cart);
                    User savedUser = userService.saveUser(user);
                    return CartDTO.convertEntityToDTO(savedUser.getCart());
                }
            } else {
                throw new ConflictException(GreenarthConstants.ServerError.USER_NOT_AUTHORIZED);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }

    @Override
    public CartDTO getCart() throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (null != user.getCart()) {
                return CartDTO.convertEntityToDTO(user.getCart());
            } else {
                throw new NotFoundException(GreenarthConstants.ServerError.CART_NOT_FOUND);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }

    @Override
    public List<ShippingOptionsByStoreDTO> getShippingOptions() throws Exception {
        User user = userService.getUser();
        if (null != user) {
            if (null != user.getCart()) {
                if (null != user.getCart().getCartItemsSet()) {
                    Map<SellerStore, List<CartItems>> mapByStore = user.getCart().getCartItemsSet().stream().collect(Collectors.groupingBy(cartItem -> cartItem.getProduct().getStore()));
                    List<ShippingOptionsByStoreDTO> shippingOptionsByStoreDTOList = new ArrayList<>();
                    for (SellerStore store : mapByStore.keySet()) {
                        ShippingOptionsByStoreDTO shippingOptionsByStoreDTO = new ShippingOptionsByStoreDTO();
                        for (CartItems cartItems : mapByStore.get(store)) {
                            CartProductDTO cartProductDTO = new CartProductDTO();
                            cartProductDTO.setProduct(ProductDTO.convertEntityToDTO(cartItems.getProduct()));
                            cartProductDTO.setQuantity(cartItems.getQuantity());
                            shippingOptionsByStoreDTO.getCartProductDTOList().add(cartProductDTO);
                        }
                        //TODO set delivery charge and delivery date in shippingOptionsByStoreDTO
                        shippingOptionsByStoreDTOList.add(shippingOptionsByStoreDTO);
                    }
                    return shippingOptionsByStoreDTOList;
                } else {
                    throw new NotFoundException(GreenarthConstants.ServerError.EMPTY_CART);
                }
            } else {
                throw new NotFoundException(GreenarthConstants.ServerError.CART_NOT_FOUND);
            }
        } else {
            throw new NotFoundException(GreenarthConstants.ServerError.USER_NOT_FOUND);
        }
    }
}
