package com.greenarth.backend.services;

import com.greenarth.backend.entities.ProductTags;
import com.greenarth.backend.entities.Products;
import com.greenarth.backend.utils.GreenarthConstants;
import com.greenarth.backend.utils.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class ProductSpecification implements Specification<Products> {

    private List<SearchCriteria> list;

    public ProductSpecification() {
        this.list = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<Products> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        // create a new predicate list
        List<Predicate> predicates = new ArrayList<>();

        // add add criteria to predicates
        for (SearchCriteria criteria : list) {
            if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.CATEGORY)) {
                if (null != criteria.getValue()) {
                    predicates.add(builder.equal(root.get("category").get("categoryId"), criteria.getValue().toString()));
                }
            } else if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.BRAND)) {
                if (null != criteria.getValue()) {
                    predicates
                            .add(builder.equal(root.get("brand"), criteria.getValue().toString()));
                }
            } else if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.BETWEEN)) {
                predicates.add(builder.between(root.get("mrp"), criteria.getMin(), criteria.getMax()));
            } else if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.CATEGORY_SEARCH)) {
                if (null != criteria.getValue()) {
                    predicates.add(builder.equal(root.get("category").get("name"), criteria.getValue().toString()));
                }
            } else if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.NAME_SEARCH)) {
                if (null != criteria.getValue()) {
                    predicates.add(builder.equal(root.get("name"), criteria.getValue().toString()));
                }
            } else if (criteria.getOperation().equals(GreenarthConstants.SearchOperation.TAG_SEARCH)) {
                Join<Products, ProductTags> productTagsJoin = root.join("productTagsSet");
                predicates.add(builder.equal(productTagsJoin.get("name"), criteria.getValue().toString()));

//                if (null != criteria.getValue()) {
//                    predicates.add(builder.equal(root.join(Products_.productTagsSet).get(ProductTags_.tagName), criteria.getValue().toString()));
//                }
            }
        }

        return builder.and(predicates.toArray(new Predicate[0]));
    }
}