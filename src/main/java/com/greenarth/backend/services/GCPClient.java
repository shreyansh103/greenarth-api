//package com.greenarth.backend.services;
//
//import com.google.auth.oauth2.GoogleCredentials;
//import com.google.cloud.storage.*;
//import com.google.common.collect.Lists;
//import org.apache.commons.io.FilenameUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//
//public class GCPClient {
//    public static String bucketName = "greenarth-bucket";
////    public static String objectPath = "test/";
//
//    public static String uploadObject(String objectName,
//                                      MultipartFile file, String objectPath) throws Exception {
//        // The ID of your GCP project
//        // String projectId = "your-project-id";
//
//        // The ID of your GCS bucket
//        // String bucketName = "your-unique-bucket-name";
//
//        // The ID of your GCS object
//        // String objectName = "your-object-name";
//
//        // The path to your file to upload
//        // String filePath = "path/to/your/file"
//        String fileName = "greenarth-development-4fd45adb86b6.json";
//        ClassLoader classLoader = GCPClient.class.getClassLoader();
//        GoogleCredentials credentials = GoogleCredentials.fromStream(classLoader.getResourceAsStream(fileName))
//                .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
//
//        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
//        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
//
//        BlobId blobId = BlobId.of(bucketName, objectPath + objectName + "." + extension);
//        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
//        blobInfo = storage.create(blobInfo, file.getBytes(), Storage.BlobTargetOption.predefinedAcl(Storage.PredefinedAcl.PUBLIC_READ));
//
//        System.out.println(
//                "File " + file.getOriginalFilename() + " uploaded to bucket " + bucketName);
//
//        return blobInfo.getMediaLink(); // Return file url
//    }
//}