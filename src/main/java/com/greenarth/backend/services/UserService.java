package com.greenarth.backend.services;

import com.greenarth.backend.DTO.UserDTO;
import com.greenarth.backend.entities.User;
import com.greenarth.backend.model.UserSignupRequest;

public interface UserService {
    UserDTO addNewUser(UserSignupRequest userSignupRequest) throws Exception;

    User getUser();

    User saveUser(User user);
}
