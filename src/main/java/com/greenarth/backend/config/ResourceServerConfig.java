package com.greenarth.backend.config;

import com.greenarth.backend.exception.CustomAccessDeniedHandler;
import com.greenarth.backend.exception.CustomAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

/**
 * The @EnableResourceServer annotation adds a filter of type
 * OAuth2AuthenticationProcessingFilter automatically to the Spring Security
 * filter chain.
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    public static final String BUYER = "BUYER";
    public static final String SELLER = "SELLER";
    public static final String ADMIN = "ADMIN";

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class).authorizeRequests()
                .antMatchers("/", "/home", "/register", "/login", "/public/**").permitAll()
                .antMatchers("/auth/buyer/**")
                .hasAuthority(BUYER)
                .antMatchers("/auth/seller/**")
                .hasAuthority(SELLER)
                .antMatchers("/auth/admin/**")
                .hasAuthority(ADMIN)
                .antMatchers("/auth/operations").hasAnyAuthority(SELLER, ADMIN)
                .antMatchers("/auth/**")
                .authenticated();

    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        // format message
        resources.authenticationEntryPoint(new CustomAuthenticationEntryPoint());
        resources.accessDeniedHandler(new CustomAccessDeniedHandler());
    }


}
