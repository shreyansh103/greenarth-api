package com.greenarth.backend.model;

public class Response {

    private int statusCode;

    private String status;

    private String message;

    private Object data;

    public Response() {
    }

    public Response(int code, String status, String message, Object data) {
        this.statusCode = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public static final class ResponseBuilder {
        private int statusCode;
        private String status;
        private String message;
        private Object data;

        private ResponseBuilder() {
        }

        public static ResponseBuilder aResponse() {
            return new ResponseBuilder();
        }

        public ResponseBuilder withStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public ResponseBuilder withStatus(String status) {
            this.status = status;
            return this;
        }

        public ResponseBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ResponseBuilder withData(Object data) {
            this.data = data;
            return this;
        }

        public Response build() {
            Response response = new Response();
            response.setStatusCode(statusCode);
            response.setStatus(status);
            response.setMessage(message);
            response.setData(data);
            return response;
        }
    }
}
