package com.greenarth.backend.model;

import java.util.ArrayList;
import java.util.List;

public class CartRequest {
    private List<CartItemRequest> cartItemRequestList = new ArrayList<CartItemRequest>();

    public List<CartItemRequest> getCartProductRequestList() {
        return cartItemRequestList;
    }

    public void setCartProductRequestList(List<CartItemRequest> cartItemRequestList) {
        this.cartItemRequestList = cartItemRequestList;
    }
}
