package com.greenarth.backend.model;

import java.util.List;

public class AddressRequestList {
    private List<AddressRequest> addressRequestList;

    public List<AddressRequest> getAddressRequestList() {
        return addressRequestList;
    }

    public void setAddressRequestList(List<AddressRequest> addressRequestList) {
        this.addressRequestList = addressRequestList;
    }
}
