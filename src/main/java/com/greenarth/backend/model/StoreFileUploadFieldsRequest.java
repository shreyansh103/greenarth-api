package com.greenarth.backend.model;

import org.springframework.web.multipart.MultipartFile;

public class StoreFileUploadFieldsRequest {
    private String fieldId;
    private MultipartFile file;

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
