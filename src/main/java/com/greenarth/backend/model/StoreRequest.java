package com.greenarth.backend.model;

import java.util.List;

public class StoreRequest {
    List<StoreInputFieldsRequest> storeInputFieldsRequestList;

    public List<StoreInputFieldsRequest> getStoreInputFieldsRequestList() {
        return storeInputFieldsRequestList;
    }

    public void setStoreInputFieldsRequestList(List<StoreInputFieldsRequest> storeInputFieldsRequestList) {
        this.storeInputFieldsRequestList = storeInputFieldsRequestList;
    }

}
