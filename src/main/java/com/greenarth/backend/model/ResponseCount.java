package com.greenarth.backend.model;

public class ResponseCount {

    private Long count;

    private Object dataList;

    public ResponseCount() {
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getDataList() {
        return dataList;
    }

    public void setDataList(Object dataList) {
        this.dataList = dataList;
    }
}
