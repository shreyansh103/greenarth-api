package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class MasterCountries extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long countryId;
    private String name;

//    @OneToMany(mappedBy = "countryId")
//    private Set<BuyerAddress> buyerAddressSet = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<Address> addressSet = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<SellerStore> sellerStoreSet = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<OrderAddress> orderAddressSet = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<MasterUIFields> masterUIFieldsSet = new HashSet<>();

    public Long getCountryId() {
        return countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public Set<OrderAddress> getOrderAddressSet() {
        return orderAddressSet;
    }

    public void setOrderAddressSet(Set<OrderAddress> orderAddressSet) {
        this.orderAddressSet = orderAddressSet;
    }

    public Set<MasterUIFields> getMasterStoreProfileFieldsSet() {
        return masterUIFieldsSet;
    }

    public void setMasterStoreProfileFieldsSet(Set<MasterUIFields> masterUIFieldsSet) {
        this.masterUIFieldsSet = masterUIFieldsSet;
    }

    public Set<SellerStore> getSellerStoreSet() {
        return sellerStoreSet;
    }

    public void setSellerStoreSet(Set<SellerStore> sellerStoreSet) {
        this.sellerStoreSet = sellerStoreSet;
    }

    public Set<MasterUIFields> getMasterUIFieldsSet() {
        return masterUIFieldsSet;
    }

    public void setMasterUIFieldsSet(Set<MasterUIFields> masterUIFieldsSet) {
        this.masterUIFieldsSet = masterUIFieldsSet;
    }
}
