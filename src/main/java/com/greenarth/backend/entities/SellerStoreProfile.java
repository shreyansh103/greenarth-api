package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class SellerStoreProfile extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long storeProfileId;

    @ManyToOne
    @JoinColumn(name = "store_id")
    private SellerStore store;

    @ManyToOne
    @JoinColumn(name = "field_id")
    private MasterUIFields storeProfileField;

    private String fieldValue;

    public long getStoreProfileId() {
        return storeProfileId;
    }

    public void setStoreProfileId(long storeProfileId) {
        this.storeProfileId = storeProfileId;
    }

    public SellerStore getStore() {
        return store;
    }

    public void setStore(SellerStore store) {
        this.store = store;
    }

    public MasterUIFields getStoreProfileField() {
        return storeProfileField;
    }

    public void setStoreProfileField(MasterUIFields storeProfileField) {
        this.storeProfileField = storeProfileField;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
