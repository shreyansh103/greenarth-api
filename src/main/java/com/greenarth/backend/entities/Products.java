package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Products extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long productId;

    private String skuCode;
    private String name;
    private String brand;
    @Lob
    private String header;
    @Column(precision = 9, scale = 4)
    private BigDecimal discount;
    @Column(precision = 9, scale = 4)
    private BigDecimal mrp;
    @Lob
    private String details;
    @Lob
    private String materialsUsed;
    @Lob
    private String howToRecycle;
    private long stock;
    private boolean active;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductImages> productImagesSet = new HashSet<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductTags> productTagsSet = new HashSet<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CartItems> cartItemsSet = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "category_id")
    private MasterCategories category;

    @ManyToOne
    @JoinColumn(name = "store_id")
    private SellerStore store;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMaterialsUsed() {
        return materialsUsed;
    }

    public void setMaterialsUsed(String materialsUsed) {
        this.materialsUsed = materialsUsed;
    }

    public String getHowToRecycle() {
        return howToRecycle;
    }

    public void setHowToRecycle(String howToRecycle) {
        this.howToRecycle = howToRecycle;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public MasterCategories getCategory() {
        return category;
    }

    public void setCategory(MasterCategories category) {
        this.category = category;
    }

    public Set<ProductImages> getProductImagesSet() {
        return productImagesSet;
    }

    public void setProductImagesSet(Set<ProductImages> productImagesSet) {
        this.productImagesSet = productImagesSet;
    }

    public Set<ProductTags> getProductTagsSet() {
        return productTagsSet;
    }

    public void setProductTagsSet(Set<ProductTags> productTagsSet) {
        this.productTagsSet = productTagsSet;
    }

    public SellerStore getStore() {
        return store;
    }

    public void setStore(SellerStore store) {
        this.store = store;
    }

    public Products addImage(ProductImages productImage) {
        productImagesSet.add(productImage);
        productImage.setProduct(this);
        return this;
    }

    public Products removeImage(ProductImages productImage) {
        productImagesSet.remove(productImage);
        productImage.setProduct(null);
        return this;
    }

    public Products addTag(ProductTags productTag) {
        productTagsSet.add(productTag);
        productTag.setProduct(this);
        return this;
    }

    public Products removeTag(ProductTags productTag) {
        productTagsSet.remove(productTag);
        productTag.setProduct(null);
        return this;
    }

    public Products addCartItem(CartItems cartItems) {
        cartItemsSet.add(cartItems);
        cartItems.setProduct(this);
        return this;
    }

    public Products removeCartItem(CartItems cartItems) {
        cartItemsSet.remove(cartItems);
        cartItems.setProduct(null);
        return this;
    }

}
