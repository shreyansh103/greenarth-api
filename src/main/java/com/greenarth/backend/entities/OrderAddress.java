package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class OrderAddress extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderAddressId;

    @OneToOne
    @JoinColumn(name = "order_id")
    private Orders order;

    private String name;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String landmark;
    private String additionalInformation;
    private String city;
    private String state;
    private String pinCode;
    private String phone;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private MasterCountries country;

}
