//package com.greenarth.backend.entities;
//
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Entity
//public class BuyerAddressData extends AuditEntity implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long addressDataId;
//
//    @ManyToOne
//    @JoinColumn(name = "field_id")
//    private MasterUIFields field;
//
//    private String value;
//
//    @ManyToOne
//    @JoinColumn(name = "buyer_address_id")
//    private BuyerAddress buyerAddress;
//
//    public BuyerAddressData() {
//    }
//
//    public long getAddressDataId() {
//        return addressDataId;
//    }
//
//    public void setAddressDataId(long addressDataId) {
//        this.addressDataId = addressDataId;
//    }
//
//    public MasterUIFields getField() {
//        return field;
//    }
//
//    public void setField(MasterUIFields field) {
//        this.field = field;
//    }
//
//    public String getValue() {
//        return value;
//    }
//
//    public void setValue(String value) {
//        this.value = value;
//    }
//
//    public BuyerAddress getBuyerAddress() {
//        return buyerAddress;
//    }
//
//    public void setBuyerAddress(BuyerAddress buyerAddress) {
//        this.buyerAddress = buyerAddress;
//    }
//}
