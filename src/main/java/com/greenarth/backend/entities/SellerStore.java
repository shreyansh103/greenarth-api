package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class SellerStore extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long storeId;
    private String name;
    private boolean enabled;
    private boolean verified;
    private String verificationStatus;
    private String comments;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "country_id", nullable = false)
    private MasterCountries country;

    @OneToMany(mappedBy = "store", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Products> productsSet = new HashSet<>();

    @OneToMany(mappedBy = "store", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SellerStoreProfile> sellerStoreProfileSet = new HashSet<SellerStoreProfile>();

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MasterCountries getCountry() {
        return country;
    }

    public void setCountry(MasterCountries country) {
        this.country = country;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    public Set<Products> getProductsSet() {
        return productsSet;
    }

    public void setProductsSet(Set<Products> productsSet) {
        this.productsSet = productsSet;
    }

    public Set<SellerStoreProfile> getSellerStoreProfileSet() {
        return sellerStoreProfileSet;
    }

    public void setSellerStoreProfileSet(Set<SellerStoreProfile> sellerStoreProfileSet) {
        this.sellerStoreProfileSet = sellerStoreProfileSet;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public SellerStore addStoreProfile(SellerStoreProfile sellerStoreProfile) {
        sellerStoreProfileSet.add(sellerStoreProfile);
        sellerStoreProfile.setStore(this);
        return this;
    }

    public SellerStore removeStoreProfile(SellerStoreProfile sellerStoreProfile) {
        sellerStoreProfileSet.remove(sellerStoreProfile);
        sellerStoreProfile.setStore(null);
        return this;
    }
}
