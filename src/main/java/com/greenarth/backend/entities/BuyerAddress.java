//package com.greenarth.backend.entities;
//
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.HashSet;
//import java.util.Set;
//
//
//@Entity
//public class BuyerAddress extends AuditEntity implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long buyerAddressId;
//
//    private String addressType;
//
//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private User user;
//
////    @ManyToOne
////    @JoinColumn(name = "country_id")
////    private MasterCountries countryId;
//
//    @OneToMany(mappedBy = "buyerAddress", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<BuyerAddressData> buyerAddressDataSet = new HashSet<>();
//
//    public BuyerAddress() {
//    }
//
//    public long getBuyerAddressId() {
//        return buyerAddressId;
//    }
//
//    public void setBuyerAddressId(long buyerAddressId) {
//        this.buyerAddressId = buyerAddressId;
//    }
//
//    public String getAddressType() {
//        return addressType;
//    }
//
//    public void setAddressType(String addressType) {
//        this.addressType = addressType;
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//
////    public MasterCountries getCountryId() {
////        return countryId;
////    }
////
////    public void setCountryId(MasterCountries countryId) {
////        this.countryId = countryId;
////    }
//
//    public BuyerAddress addBuyerAddressData(BuyerAddressData addressData) {
//        buyerAddressDataSet.add(addressData);
//        addressData.setBuyerAddress(this);
//        return this;
//    }
//
//    public BuyerAddress removeBuyerAddressData(BuyerAddressData addressData) {
//        buyerAddressDataSet.remove(addressData);
//        addressData.setBuyerAddress(null);
//        return this;
//    }
//}
