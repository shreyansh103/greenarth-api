package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MasterUIFields extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long fieldId;
    private String label;
    private String placeHolder;
    private String regex;
    private String type;
    private String viewOrder;
    private boolean required;

    @ManyToOne
    @JoinColumn(name = "section_id")
    private MasterUISections section;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private MasterCountries country;

    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MasterUISections getSection() {
        return section;
    }

    public void setSection(MasterUISections section) {
        this.section = section;
    }

    public MasterCountries getCountry() {
        return country;
    }

    public void setCountry(MasterCountries country) {
        this.country = country;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(String viewOrder) {
        this.viewOrder = viewOrder;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
