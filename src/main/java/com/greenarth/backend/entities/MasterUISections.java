package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class MasterUISections extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long sectionId;
    private String name;
    private String viewOrder;

    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<MasterUIFields> masterUIFieldsSet;

    public long getSectionId() {
        return sectionId;
    }

    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MasterUIFields> getMasterStoreProfileFieldsSet() {
        return masterUIFieldsSet;
    }

    public void setMasterStoreProfileFieldsSet(Set<MasterUIFields> masterUIFieldsSet) {
        this.masterUIFieldsSet = masterUIFieldsSet;
    }

    public String getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(String viewOrder) {
        this.viewOrder = viewOrder;
    }
}
