package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CartItems extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cartItemsId;

    @ManyToOne
    @JoinColumn
    private Cart cart;

    @ManyToOne
    @JoinColumn
    private Products product;

    private long quantity;

    public Long getCartItemsId() {
        return cartItemsId;
    }

    public void setCartItemsId(Long cartItemsId) {
        this.cartItemsId = cartItemsId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
