package com.greenarth.backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class MasterCategories extends AuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long categoryId;
    private String name;
    private String description;

    @OneToMany(mappedBy = "category")
    private Set<Products> productsSet;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
