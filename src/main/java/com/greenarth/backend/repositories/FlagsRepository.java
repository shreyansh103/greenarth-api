package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.MasterFlags;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlagsRepository extends JpaRepository<MasterFlags, Long> {

    MasterFlags getByKey(String launched);
}
