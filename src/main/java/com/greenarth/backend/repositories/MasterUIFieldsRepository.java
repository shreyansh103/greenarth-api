package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.MasterCountries;
import com.greenarth.backend.entities.MasterUIFields;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MasterUIFieldsRepository extends JpaRepository<MasterUIFields, Long> {

    List<MasterUIFields> findAllByCountry(MasterCountries country);

    List<MasterUIFields> findAllByCountryAndType(MasterCountries country, String type);
}
