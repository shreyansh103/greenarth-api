package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.Blogs;
import com.greenarth.backend.entities.SellerStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellerStoreRepository extends JpaRepository<SellerStore, Long> {

//    boolean existsByGstNumber(String gstNumber);
//
//    boolean existsByPanNumber(String panNumber);
}
