package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.Blogs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogsRepository extends JpaRepository<Blogs, Long> {

}
