package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.MasterCountries;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterCountriesRepository extends JpaRepository<MasterCountries, Long> {

}
