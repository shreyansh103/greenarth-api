package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.Products;
import com.greenarth.backend.entities.SellerStore;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Products, Long>, JpaSpecificationExecutor<Products> {

    Page<Products> findAllByStore(SellerStore sellerStore, Pageable paging);

    long countByStore(SellerStore sellerStore);

    @Query(value = "select DISTINCT brand from products", nativeQuery = true)
    List<String> findAllBrands();

    Products findBySkuCode(String skuCode);
}
