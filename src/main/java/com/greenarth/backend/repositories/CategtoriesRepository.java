package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.MasterCategories;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategtoriesRepository extends JpaRepository<MasterCategories, Long> {

}
