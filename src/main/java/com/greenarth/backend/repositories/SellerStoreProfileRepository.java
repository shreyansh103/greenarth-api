package com.greenarth.backend.repositories;

import com.greenarth.backend.entities.MasterUIFields;
import com.greenarth.backend.entities.SellerStore;
import com.greenarth.backend.entities.SellerStoreProfile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SellerStoreProfileRepository extends JpaRepository<SellerStoreProfile, Long> {

    SellerStoreProfile findByStoreAndStoreProfileField(SellerStore sellerStore, MasterUIFields masterUIFields);
}
