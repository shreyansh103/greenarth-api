package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.User;

public class UserDTO {
    public String userId;
    public String email;
    public String role;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserDTO convertEntityToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(String.valueOf(user.getUserId()));
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(user.getRole().getName());
        return userDTO;
    }
}
