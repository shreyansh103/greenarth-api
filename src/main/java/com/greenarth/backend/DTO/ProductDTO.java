package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.ProductImages;
import com.greenarth.backend.entities.Products;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDTO extends CategoryDTO {

    private Long productId;
    private String skuCode;
    private String name;
    private String brand;
    private String header;
    private String mrp;
    private String discount;
    private String details;
    private String materialsUsed;
    private String howToRecycle;
    private long stock;
    private boolean active;
    private List<String> productImages = new ArrayList<String>();

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMaterialsUsed() {
        return materialsUsed;
    }

    public void setMaterialsUsed(String materialsUsed) {
        this.materialsUsed = materialsUsed;
    }

    public String getHowToRecycle() {
        return howToRecycle;
    }

    public void setHowToRecycle(String howToRecycle) {
        this.howToRecycle = howToRecycle;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<String> productImages) {
        this.productImages = productImages;
    }

    public static ProductDTO convertEntityToDTO(Products savedProduct) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductId(savedProduct.getProductId());
        productDTO.setActive(savedProduct.isActive());
        productDTO.setBrand(savedProduct.getBrand());
        productDTO.setDetails(savedProduct.getDetails());
        productDTO.setDiscount(null != savedProduct.getDiscount() ? savedProduct.getDiscount().stripTrailingZeros().toPlainString() : null);
        productDTO.setHeader(savedProduct.getHeader());
        productDTO.setHowToRecycle(savedProduct.getHowToRecycle());
        productDTO.setCategoryName(savedProduct.getName());
        productDTO.setMrp(null != savedProduct.getMrp() ? savedProduct.getMrp().stripTrailingZeros().toPlainString() : null);
        productDTO.setMaterialsUsed(savedProduct.getMaterialsUsed());
        productDTO.setName(savedProduct.getName());
        productDTO.setStock(savedProduct.getStock());
        productDTO.setSkuCode(savedProduct.getSkuCode());
        if (savedProduct.getProductImagesSet().size() > 0) {
            List<ProductImages> productImagesList = savedProduct.getProductImagesSet().stream().sorted((e1, e2) ->
                    e1.getImageId().compareTo(e2.getImageId())).collect(Collectors.toList());
            for (ProductImages productImage : productImagesList) {
                productDTO.getProductImages().add(productImage.getImageURL());
            }
        }
        if (null != savedProduct.getCategory()) {
            productDTO.setCategoryId(savedProduct.getCategory().getCategoryId());
            productDTO.setCategoryName(savedProduct.getCategory().getName());
            productDTO.setCategoryDescription(savedProduct.getCategory().getDescription());
        }
        return productDTO;
    }


}
