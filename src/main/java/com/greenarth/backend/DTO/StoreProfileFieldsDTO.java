package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.MasterUIFields;

public class StoreProfileFieldsDTO {
    private long fieldId;
    private String label;
    private String placeHolder;
    private String regex;
    private String type;
    private String viewOrder;
    private boolean required;

    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(String viewOrder) {
        this.viewOrder = viewOrder;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static StoreProfileFieldsDTO convertEntityToDTO(MasterUIFields masterUIFields) {
        StoreProfileFieldsDTO storeProfileFieldsDTO = new StoreProfileFieldsDTO();
        storeProfileFieldsDTO.setFieldId(masterUIFields.getFieldId());
        storeProfileFieldsDTO.setLabel(masterUIFields.getLabel());
        storeProfileFieldsDTO.setType(masterUIFields.getType());
        storeProfileFieldsDTO.setPlaceHolder(masterUIFields.getPlaceHolder());
        storeProfileFieldsDTO.setRegex(masterUIFields.getRegex());
        storeProfileFieldsDTO.setRequired(masterUIFields.isRequired());
        storeProfileFieldsDTO.setViewOrder(masterUIFields.getViewOrder());
        return storeProfileFieldsDTO;
    }

}
