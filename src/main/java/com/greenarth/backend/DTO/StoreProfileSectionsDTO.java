package com.greenarth.backend.DTO;

import java.util.List;

public class StoreProfileSectionsDTO extends CountryDTO {
    private long sectionId;
    private String name;
    private String viewOrder;
    private List<StoreProfileFieldsDTO> fieldList;

    public long getSectionId() {
        return sectionId;
    }

    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(String viewOrder) {
        this.viewOrder = viewOrder;
    }

    public List<StoreProfileFieldsDTO> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<StoreProfileFieldsDTO> fieldList) {
        this.fieldList = fieldList;
    }
}
