package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.SellerStore;
import com.greenarth.backend.entities.SellerStoreProfile;
import com.greenarth.backend.services.AmazonClient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SellerStoreDTO extends UserDTO {
    private long storeId;
    private String verificationStatus;
    private boolean enabled;
    private boolean verified;
    private String comments;
    private Timestamp submittedOn;
    List<SellerStoreProfileDTO> sellerStoreProfileDTOList = new ArrayList<>();

    public long getStoreId() {
        return storeId;
    }

    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Timestamp getSubmittedOn() {
        return submittedOn;
    }

    public void setSubmittedOn(Timestamp submittedOn) {
        this.submittedOn = submittedOn;
    }

    public List<SellerStoreProfileDTO> getSellerStoreProfileDTOList() {
        return sellerStoreProfileDTOList;
    }

    public void setSellerStoreProfileDTOList(List<SellerStoreProfileDTO> sellerStoreProfileDTOList) {
        this.sellerStoreProfileDTOList = sellerStoreProfileDTOList;
    }

    public static SellerStoreDTO convertEntityToDTO(SellerStore sellerStore, AmazonClient amazonClient) {
        SellerStoreDTO sellerStoreDTO = new SellerStoreDTO();
        sellerStoreDTO.setStoreId(sellerStore.getStoreId());
        sellerStoreDTO.setEnabled(sellerStore.isEnabled());
        sellerStoreDTO.setVerified(sellerStore.isVerified());
        sellerStoreDTO.setVerificationStatus(sellerStore.getVerificationStatus());
        sellerStoreDTO.setComments(sellerStore.getComments());
        sellerStoreDTO.setSubmittedOn(sellerStore.getModificationTime());
        sellerStoreDTO.setEmail(sellerStore.getUser().getEmail());
        sellerStoreDTO.setUserId(String.valueOf(sellerStore.getUser().getUserId()));
        sellerStoreDTO.setRole(sellerStore.getUser().getRole().getName());
        List<SellerStoreProfileDTO> storeProfileDTOList = new ArrayList<>();
        for (SellerStoreProfile sellerStoreProfile : sellerStore.getSellerStoreProfileSet()) {
            SellerStoreProfileDTO sellerStoreProfileDTO = new SellerStoreProfileDTO();
            sellerStoreProfileDTO.setProfileId(sellerStoreProfile.getStoreProfileId());
            sellerStoreProfileDTO.setFieldId(sellerStoreProfile.getStoreProfileField().getFieldId());
            sellerStoreProfileDTO.setFieldLabel(sellerStoreProfile.getStoreProfileField().getLabel());
            sellerStoreProfileDTO.setFieldType(sellerStoreProfile.getStoreProfileField().getType());
            if (sellerStoreProfile.getStoreProfileField().getType().equalsIgnoreCase("file upload")) {
                sellerStoreProfileDTO.setFieldValue(amazonClient.generatePreSignedUrl(sellerStoreProfile.getFieldValue()));
            } else {
                sellerStoreProfileDTO.setFieldValue(sellerStoreProfile.getFieldValue());
            }
            storeProfileDTOList.add(sellerStoreProfileDTO);
        }
        sellerStoreDTO.getSellerStoreProfileDTOList().addAll(storeProfileDTOList);
        return sellerStoreDTO;
    }


}
