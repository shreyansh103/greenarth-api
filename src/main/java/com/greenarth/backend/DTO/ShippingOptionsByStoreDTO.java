package com.greenarth.backend.DTO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ShippingOptionsByStoreDTO {
    private List<CartProductDTO> cartProductDTOList = new ArrayList<>();
    private Timestamp deliveryDate;
    private String deliveryCharge;

    public List<CartProductDTO> getCartProductDTOList() {
        return cartProductDTOList;
    }

    public void setCartProductDTOList(List<CartProductDTO> cartProductDTOList) {
        this.cartProductDTOList = cartProductDTOList;
    }

    public Timestamp getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Timestamp deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }


}
