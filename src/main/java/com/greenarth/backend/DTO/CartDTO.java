package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.Cart;
import com.greenarth.backend.entities.CartItems;

import java.util.ArrayList;
import java.util.List;

public class CartDTO {
    private long cartId;
    private List<CartProductDTO> cartProductRequestList = new ArrayList<CartProductDTO>();

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public List<CartProductDTO> getCartProductRequestList() {
        return cartProductRequestList;
    }

    public void setCartProductRequestList(List<CartProductDTO> cartProductRequestList) {
        this.cartProductRequestList = cartProductRequestList;
    }

    public static CartDTO convertEntityToDTO(Cart savedCart) {
        CartDTO cartDTO = new CartDTO();
        cartDTO.setCartId(savedCart.getCartId());
        for (CartItems cartItems : savedCart.getCartItemsSet()) {
            CartProductDTO cartProductDTO = new CartProductDTO();
            cartProductDTO.setProduct(ProductDTO.convertEntityToDTO(cartItems.getProduct()));
            cartProductDTO.setQuantity(cartItems.getQuantity());
            cartDTO.getCartProductRequestList().add(cartProductDTO);
        }
        return cartDTO;
    }
}
