package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.MasterCategories;

import java.util.ArrayList;
import java.util.List;

public class FiltersDTO {

    private List<CategoryDTO> categoryDTOList = new ArrayList<>();
    private List<String> brandList = new ArrayList<>();

    public List<CategoryDTO> getCategoryDTOList() {
        return categoryDTOList;
    }

    public void setCategoryDTOList(List<CategoryDTO> categoryDTOList) {
        this.categoryDTOList = categoryDTOList;
    }

    public List<String> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<String> brandList) {
        this.brandList = brandList;
    }

    public static FiltersDTO convertEntityToDTO(List<MasterCategories> categoriesList, List<String> brandList) {
        FiltersDTO filtersDTO = new FiltersDTO();
        for (MasterCategories category : categoriesList) {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setCategoryId(category.getCategoryId());
            categoryDTO.setCategoryName(category.getName());
            categoryDTO.setCategoryDescription(category.getDescription());
            filtersDTO.getCategoryDTOList().add(categoryDTO);
        }
        filtersDTO.getBrandList().addAll(brandList);
        return filtersDTO;
    }


}
