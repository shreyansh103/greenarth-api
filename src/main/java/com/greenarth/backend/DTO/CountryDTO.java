package com.greenarth.backend.DTO;

import com.greenarth.backend.entities.MasterCountries;

public class CountryDTO {
    private long countryId;
    private String countryName;

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public static CountryDTO convertEntityToDTO(MasterCountries country) {
        CountryDTO countryDTO = new CountryDTO();
        countryDTO.setCountryId(country.getCountryId());
        countryDTO.setCountryName(country.getName());
        return countryDTO;
    }
}
