package com.greenarth.backend.DTO;


import com.greenarth.backend.entities.Address;

public class AddressDTO extends CountryDTO {
    private long addressId;
    private String name;
    private String addressType;
    private String addressTag;
    private String address;
    private String streetAddress;
    private String apartment;
    private String landmark;
    private String additionalInformation;
    private String city;
    private String state;
    private String pinCode;
    private String zipCode;
    private boolean billingSame;
    private String phone;

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressTag() {
        return addressTag;
    }

    public void setAddressTag(String addressTag) {
        this.addressTag = addressTag;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public boolean isBillingSame() {
        return billingSame;
    }

    public void setBillingSame(boolean billingSame) {
        this.billingSame = billingSame;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public static AddressDTO convertEntityToDTO(Address savedAddress) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddressId(savedAddress.getAddressId());
        addressDTO.setAddressType(savedAddress.getAddressType());
        addressDTO.setAddress(null != savedAddress.getAddress() ? savedAddress.getAddress() : null);
        addressDTO.setStreetAddress(null != savedAddress.getStreetAddress() ? savedAddress.getStreetAddress() : null);
        addressDTO.setApartment(null != savedAddress.getApartment() ? savedAddress.getApartment() : null);
        addressDTO.setAddressTag(null != savedAddress.getAddressTag() ? savedAddress.getAddressTag() : null);
        addressDTO.setAdditionalInformation(null != savedAddress.getAdditionalInformation() ? savedAddress.getAdditionalInformation() : null);
        addressDTO.setCity(null != savedAddress.getCity() ? savedAddress.getCity() : null);
        addressDTO.setLandmark(null != savedAddress.getLandmark() ? savedAddress.getLandmark() : null);
        addressDTO.setState(null != savedAddress.getState() ? savedAddress.getState() : null);
        addressDTO.setPinCode(null != savedAddress.getPinCode() ? savedAddress.getPinCode() : null);
        addressDTO.setZipCode(null != savedAddress.getZipCode() ? savedAddress.getZipCode() : null);
        addressDTO.setPhone(null != savedAddress.getPhone() ? savedAddress.getPhone() : null);
        addressDTO.setName(null != savedAddress.getName() ? savedAddress.getName() : null);
        addressDTO.setBillingSame(savedAddress.isBillingSame());
        addressDTO.setCountryId(savedAddress.getCountry().getCountryId());
        addressDTO.setCountryName(savedAddress.getCountry().getName());
        return addressDTO;
    }

}